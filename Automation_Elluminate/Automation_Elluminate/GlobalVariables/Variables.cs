﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RelevantCodes.ExtentReports;

namespace Automation_Elluminate
{
    
    class ElementInfo
    {
        public static IWebElement webElement = null;
        public static IWebDriver driver = null;
        public static By by = null;        
    }
    class Variables
    {
        public static Report Reportobj = new Report();
        public static string TestCaseStatus = String.Empty;
        public static string locatorType = String.Empty;
        public static string ProjectPath = String.Empty;
        public static string applicationName = "Elluminate";    
        public static Hashtable DataSetTable = new Hashtable();
        public static ArrayList DataSetkey = new ArrayList();
        public static ArrayList DataSetvalues = new ArrayList();
        public static int iterationCount = 0;
        public static int iteration = 0;
        public static string currentScenarioName = String.Empty;        
        public static Boolean isTestCaseFailed=false;     
        public static string ScreenShotLocation;                      
        public static string dateTimeFormat = "HHmmss";
        public static string screenshotExtension = ".jpg";
        public static string dateFormat = "ddMMMyyyy";       
        public static string reportExtension = ".html";
        public static string testCaseRunStatus = "Y";
        public static string RandomString = null;

    }

    class Report
    {
        public static ExtentReports extent;
        public static ExtentTest test;
        public static String ErrorMessage = null;
        public static String reportPath = @"C:\\Reports\Screenshot\"+DateTime.Now.ToString("ddMMMyyyy") +"\\";
        public static String TestReprot = null;

    }

    class LoginUser1
    {
        public static string Username = "AutoTesting";
        public static string Password = "Auto@403";
        public static string ProfileName = "SmokeTest, Auto";
        
    }
    class LoginUser2
    {
        public static string Username = "AutoUser";
        public static string Password = "Auto@user";
        public static string ProfileName = "Testing, TestAuto";

    }


}
