﻿Feature: Elluminate_eDrive
	
	Background:
	Given We are at the Sign In Page
	And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"

Scenario:Elluminate_VersionControlFolder_Creation_SmokeTest 
	Given I choose "MapperTest" Study from studies list.
	Given select eDrive Tab 
	Then  I should see eDrive home page
	When I Create new folder called "Test" for "VersionControl" type
	Then Verify "Test" under folders section
	And Upload a file for "Test" folder for "VersionControl"
	When I Create new folder called "Test1" for "VersionControl" type
	And Click on "Test" folder
	When I Create new folder called "Test2" for "NonVersionControl" type
	Then Expand "Test" folder
	Then Verify "Test1" under folders section
	Then Verify "Test2" under folders section
	Then Delete "Test" folder
	 

Scenario: Elluminate_Folder_Modification_SmokeTest  
    Given I choose "MapperTest" Study from studies list.
    Given select eDrive Tab 
    Then  I should see eDrive home page
    When I Create new folder called "Test" for "VersionControl" type
    Then Verify "Test" under folders section 
	When Click on "Test" folder
    Then I Rename Folder "Test" to "RenameTest"
    Then Delete "RenameTest" folder
    And Verify deleted folder "RenameTest" 

Scenario: Elluminate_VersionControlFolder_FileActions_SmokeTest
    Given I choose "MapperTest" Study from studies list.
	Given select eDrive Tab 
	Then  I should see eDrive home page
	When I Create new folder called "Test" for "VersionControl" type
	Then Verify "Test" under folders section
	And Upload a file for "Test" folder for "VersionControl"
	Then I Copy the "cm.xlsx" file to "Global" folder
	And Then I Download the file
	Then Move the "cm.xlsx" file to "MapperTest" folder
	And I Rename the file "cm.xlsx" to "domain.xlsx"
	Then I Delete the file "cm.xlsx"
	Then Delete "Test" folder

Scenario: Elluminate_NonVersionControlFolder_FileActions_SmokeTest
    Given I choose "MapperTest" Study from studies list.
	Given select eDrive Tab 
	Then  I should see eDrive home page
	When I Create new folder called "StudyFolder" for "NonVersionControl" type
	Then Upload a file for "StudyFolder" folder for "NonVersionControl"
	Then I Copy the "cm.xlsx" file to "Global" folder
	Then Then I Download the file
	Then Move the "cm.xlsx" file to "MapperTest" folder
	And I Rename the file "cm.xlsx" to "domain.xlsx"
	Then I Delete the file "cm.xlsx"
	Then Delete "StudyFolder" folder
	
Scenario: Elluminate_VersionControlFolder_CheckOut_SmokeTest
	Given I choose "MapperTest" Study from studies list.
	Given select eDrive Tab 
	Then  I should see eDrive home page
	When I Create new folder called "Test" for "VersionControl" type
	Then Verify "Test" under folders section
	And Upload a file for "Test" folder for "VersionControl"
	Then Perform check out
	And Undo check out 
	
Scenario: Elluminate_VersionControlFolder_CheckIn_SmokeTest
	Given I choose "MapperTest" Study from studies list.
	Given select eDrive Tab 
	Then  I should see eDrive home page
	When I Create new folder called "Test" for "VersionControl" type
	Then Verify "Test" under folders section
	And Upload a file for "Test" folder for "VersionControl"
	Then Perform check in
	
	