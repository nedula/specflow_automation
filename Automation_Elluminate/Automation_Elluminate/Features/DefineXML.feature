﻿Feature: Elluminate_DefineXML
	
Background:  
   Given We are at the Sign In Page
   And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"
   Given I choose "DataQty" Study from studies list.

 Scenario: Elluminate_DefineXML_UploadSpecFile_Validate_SmokeTest
	Given Go to DefineXML tab
	Then Upload a Spec file
	And Generate file with Validating
	And Define xml should be generated with Validation results is "true" 

Scenario: Elluminate_DefineXML_UploadSpecFile_WithoutValidate_SmokeTest
	Given Go to DefineXML tab
	Then  Upload a Spec file
	And Generate file without Validating
	And Define xml should be generated with Validation results is "false" 

Scenario: Elluminate_DefineXML_ViewValidationResults_DataQuality_SmokeTest
    Given Go to Data Quality tab
	Then Verify DataQuality Validation Result
	Given Go to DefineXML tab
	Then  Upload a Spec file
	And Generate file with Validating
	And Validate and Generate for Validation is "true"
	Given Go to Data Quality tab
	Then Verify DataQuality Validation Result

Scenario: Elluminate_DefineXML_DownloadXML_SmokeTest
    Given Go to Data Quality tab
	Then Verify DataQuality Validation Result
	Given Go to DefineXML tab
	Then  Upload a Spec file
	And Generate file with Validating
	And Validate and Generate for Validation is "true"
	Then Verify Validation result link in DefineXML
	Given Go to Data Quality tab
	Then Verify DataQuality Validation Result
	And Download XML file from Data Quality page