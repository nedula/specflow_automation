﻿Feature: Elluminate_DataQuality
	
Background:  
   Given We are at the Sign In Page
   And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"
   Given I choose "MapperTest" Study from studies list.

   
Scenario:Elluminate_Create_DataQualityValidation_SmokeTest 
	Given select Data Quality Tab 
	Then  I should see Data Quality home page
	And Clear the DataQuality data
	Then Select the Tile view
	And Create "DataQuality1" DataQuality Validation for "Clinical" DataStore and "CM" domain

Scenario:Elluminate_DataQualityValidation_Actions_SmokeTest 
	Given select Data Quality Tab 
	Then I should see Data Quality home page
	And Clear the DataQuality data
	Then Select the Tile view
	And Create "DataQuality1" DataQuality Validation for "Clinical" DataStore and "CM" domain
	Then Run the Data Quality job
	And Edit the Data Quality job as "DataQuality2" DataQuality Validation for "ODM_Stage" DataStore and "Mapper_Domain" domain
	Then Schedule the Data Quality task for "OneTime"
	Given select Data Quality Tab 
	Then Delete the "DataQuality2" DataQuaity job 

Scenario:Elluminate_Validate_Domains_SmokeTest 
	Given select Data Quality Tab 
	Then Go to Validations tab
	And Select "Clinical" DataStore and "SE" domain
	Then Validate Domains

Scenario:Elluminate_Validate_Domains_Cancel_SmokeTest 
	Given select Data Quality Tab 
	Then Go to Validations tab
	And Select "Clinical" DataStore and "SE" domain
	Then Cancel Validate Domains

Scenario:Elluminate_Validate_Domains_Delete_SmokeTest 
	Given select Data Quality Tab 
	Then Go to Validations tab
	And Delete the validation job

Scenario:Elluminate_ValidationResults_Contents_SmokeTest 
    Given select Data Quality Tab
	Then Go to Validation Results tab
	And Verify data store exists and Domain checked
	Then Verify result records filtered using Information, Warning and Error 