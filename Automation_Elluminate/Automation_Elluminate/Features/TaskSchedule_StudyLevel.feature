﻿Feature: Elluminate_TaskSchedule_StudyLevel
	

Background: 
   Given We are at the Sign In Page
   And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"
   Given I choose "AutoStudy" Study from studies list.  
   When I go to Importer tab
   Then I Clear the data within Importer tab
    Given Go to Data Sources tab
   Given Clear the data within Data Sources tab
  


Scenario: Elluminate_TaskSchedule_Import_SmokeTest
   When I create a new "FTP" Data Source with below details
   | Name        | AddressUrl                   | UserName | Password   | Remotefoldername | Remotefiles  | Description         |
   | Auto_DS_FTP | ftpes://ftp.eclinicalsol.com | cdara    | password.1 | Chandu           | TestData.zip | FTP Test Descrption |
   When I go to Importer tab
   Then I create import definition for "FTP" with Data Source is "Auto_DS_FTP" and DataStore is "Clinical"
   Given Select Task tab 
   And click on Add Task link
   Then Created Task With name "ImportNew" , Type "OneTime" , Action "Import"
   When I Edit Task  Name "ImportNew" to "ImportEdit" , Type "OneTime" , Action "Import"  
   When I Run "ImportEdit" Task
   Then Verify "ImportEdit" Task is Executed successfully 
   When I Delete "ImportEdit" Task   
   Then I Deleted the Import definition and related data source  