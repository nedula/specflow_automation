﻿Feature: Elluminate_Import

Background: 
   Given We are at the Sign In Page
   And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"
   Given I choose "AutoStudy" Study from studies list.  
   When I go to Importer tab
   Then I Clear the data within Importer tab
    Given Go to Data Sources tab
   Given Clear the data within Data Sources tab
  


Scenario: Elluminate_Import_DS_FTP_SmokeTest
   When I create a new "FTP" Data Source with below details
   | Name        | AddressUrl                   | UserName | Password   | Remotefoldername | Remotefiles  | Description         |
   | Auto_DS_FTP | ftpes://ftp.eclinicalsol.com | cdara    | password.1 | Chandu           | TestData.zip | FTP Test Descrption |
   When I go to Importer tab
   Then I create import definition for "FTP" with Data Source is "Auto_DS_FTP" and DataStore is "Clinical"
   When I Select import definition and run 
   Then Files started Importing and Wait until status is Read to integrate  
   And Click on Integrate button and Wait until status is Completed
   And Verify import definitions status in History Tab
   And I Deleted the Import definition and related data source  




Scenario: Elluminate_Import_DS_MediroWebService_SmokeTest
   When I create a new "MediroWebService" Data Source with below details
   | Name             | AddressUrl                 |apikey               | Description                         |
   | Auto_MediroWS_DS | https://na7.api.medrio.com| c8Ba0mPhYeHK6w4eCsz1 | Mediro Web Service  Test Descrption |
   When I go to Importer tab
   Then I create import definition for "MediroWebService" with Data Source is "Auto_MediroWS_DS" and DataStore is "Clinical"
   When I Select import definition and run 
   Then Files started Importing and Wait until status is Read to integrate  
   And Click on Integrate button and Wait until status is Completed
   And Verify import definitions status in History Tab
   And I Deleted the Import definition and related data source  


   

Scenario: Elluminate_Import_DS_MergeeCLinicalOSWebServices_SmokeTest
   When I create a new "MergeeCLinicalOSWebServices" Data Source with below details
   | Name             | AddressUrl                                        | UserName |Password    | RemoteStudy| Description |
   | Auto_MergeEWS_DS | https://api.eclinicalos.com/edc_studyservices.jsp | esiroka  | Password.7 | 5346       | Merge eCLinicalOS Web Services Test Descrption |
   When I go to Importer tab
   Then I create import definition for "MergeeCLinicalOSWebServices" with Data Source is "Auto_MergeEWS_DS" and DataStore is "Clinical"
   When I Select import definition and run 
   Then Files started Importing and Wait until status is Read to integrate  
   And Click on Integrate button and Wait until status is Completed
   And Verify import definitions status in History Tab
   And I Deleted the Import definition and related data source  

Scenario: Elluminate_Import_DS_RaveBioStatsGateway_SmokeTest
   When I create a new "RaveBioStatsGateway" Data Source with below details
   | Name            | AddressUrl                               | UserName  | Password   | RemoteStudy |Environment| Description |
   | Auto_RaveBSG_DS | https://halo-e.mdsol.com/RaveWebServices | ECLIN_RWS | password.1 | HALO-DMD-01 | UAT         |Rave BioStats Gateway Test Descrption|
   When I go to Importer tab
   Then I create import definition for RaveBioStatsGateway 
    When I Select import definition and run 
   Then Files started Importing and Wait until status is Read to integrate  
   And Click on Integrate button and Wait until status is Completed
   And Verify import definitions status in History Tab
   And I Deleted the Import definition and related data source  

Scenario: Elluminate_Import_DS_RaveODMAdaptor_SmokeTest
   When I create a new "RaveODMAdaptor" Data Source with below details
   | Name            | AddressUrl                               | UserName  | Password   | RemoteStudy |Environment| Description |
   | Auto_RaveODM_DS | https://halo-e.mdsol.com/RaveWebServices | ECLIN_RWS | password.1 | HALO-DMD-01 | UAT         |Rave ODM Adaptor Test Descrption|
   When I go to Importer tab
   Then I create import definition for RaveODMAdaptor 
    When I Select import definition and run 
   Then Files started Importing and Wait until status is Read to integrate  
   And Click on Integrate button and Wait until status is Completed
   And Verify import definitions status in History Tab
   And I Deleted the Import definition and related data source  

Scenario: Elluminate_Import_DS_RaveWebServices_SmokeTest
   When I create a new "RaveWebServices" Data Source with below details
   | Name            | AddressUrl                                          | UserName  | Password   | RemoteStudy |Environment| Description |
   | Auto_RaveWS_DS | https://eclinicalsolutions.mdsol.com/RaveWebServices | ECLIN_RWS1 | password.1 | eCLINICAL-DEMO | DEMO   |Rave Web Services Test Descrption|
   When I go to Importer tab
   Then I create import definition for RaveWebServices 
   When I Select import definition and run 
   Then Files started Importing and Wait until status is Read to integrate  
   And Click on Integrate button and Wait until status is Completed
   And Verify import definitions status in History Tab
   And I Deleted the Import definition and related data source  

Scenario: Elluminate_Import_File_SmokeTest
   When I go to Importer tab
   Then I create import definition for File 
   Then I Run by uploading file

Scenario: Elluminate_Import_ManualFileImport_SmokeTest
   When I go to Importer tab
   Then I perform Manual File Import

 Scenario: Elluminate_Import_Schedule_DS_FTP
   When I create a new "FTP" Data Source with below details
   | Name        | AddressUrl                   | UserName | Password   | Remotefoldername | Remotefiles  | Description         |
   | Auto_DS_FTP | ftpes://ftp.eclinicalsol.com | cdara    | password.1 | Chandu           | TestData.zip | FTP Test Descrption |
   When I go to Importer tab
   Then I create import definition for FTP
   Then I Schedule Import for FTP