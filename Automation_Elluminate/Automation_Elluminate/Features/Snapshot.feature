﻿Feature: Elluminate_Sanpshot
	
Background:  
   Given We are at the Sign In Page
   And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"
   Given I choose "AutoStudy" Study from studies list.
   

Scenario: Elluminate_Snapshot_Add_SmokeTest
	Given Go to Snapshot tab
	Then create new Snapshot

Scenario: Elluminate_Snapshot_Edit_SmokeTest
	Given Go to Snapshot tab
	Then Edit the Snapshot
	Then verify snapshot title "Snapshot_Nov_2017"

Scenario: Elluminate_Snapshot_Delete_SmokeTest
	Given Go to Snapshot tab
	Then Delete the Snapshot
	

