﻿Feature: Elluminate_Configuration
	In Adminstrative Configuration verifying compound,stagingArea and DataMart. 
	Below are the scenarios to CREATE/EDIT/DELETE compound, stagingArea and DataMart

Background:
    Given We are at the Sign In Page
	And I login "AutoTesting", "Auto@403" and "SmokeTest, Auto"
    

Scenario:Elluminate_Compound_SmokeTest
	When I create a new compound called "New Compoundr"
	Then I should see compound"New Compoundr" 
	When I edit a compound called "New Compoundr" to "Edited Compound"
	Then I should not see Compound "New Compoundr"
	And I should see compound"Edited Compound"
	When I delete a compound called "Edited Compound"
	Then I should not see Compound "Edited Compound"

	Scenario:Elluminate_StagingArea_SmokeTest
	When I create a new StagingArea called "New StagingArea"
	Then I should see StagingArea "New StagingArea" 
	And  I edit a StagingArea called "New StagingArea" to "Edited StagingArea"
	Then I should not see StagingArea "New StagingArea"
	And  I should see StagingArea "Edited StagingArea"
	When I delete a StagingArea called "Edited StagingArea"
	Then I should not see StagingArea "Edited StagingArea"

	Scenario:Elluminate_DataMart_SmokeTest
	When I create a new DataMart called "New DataMart"
	Then I should see DataMart "New DataMart" 
	And  I edit a DataMart called "New DataMart" to "Edited DataMart"
	Then I should not see DataMart "New DataMart"
	And I should see DataMart "Edited DataMart"
	When I delete a DataMart called "Edited DataMart"
	Then I should not see DataMart "Edited DataMart"
