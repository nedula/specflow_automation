﻿Feature:Elluminate_Administration
	
	
Scenario:Elluminate_User_SmokeTest 
	Given We are at the Sign In Page
	And I login with user called "AutoTesting"
	Given Go to Administration, User Management
	When I create a new User with below deatils
	|   UserName   |FirstName|  Company   |LastName|Email|
	|AutomationUser|FirstName|eClinicalSol|LastName|Autotesting@eclinicalsol.com|	
	Then I should see user  called "AutomationUser" in user list
	When I Edit user called "AutomationUser" with below details
	|UserName|FirstName|Company|LastName|Email|
	|AutomationUser|EditFirstName|eClinicalSolution|EditLastName|Autotesting@eclinicalsol.com|
	Then I should see user  called "AutomationUser" in user list
	When I Delete user called "AutomationUser"
	Then I should not see user called "AutomationUser"
	    
Scenario:Elluminate_User_Disable_SmokeTest 
	Given We are at the Sign In Page
   And I login with user called "AutoTesting"
   Given Go to Administration, User Management
   And I should able to disable the user called "AUTOUSER"
   When I Logout from current user
   And I login with disable user called "AUTOUSER"
   Then I should see message "Your account has been disabled. Please contact eClinical helpdesk."
   Given I login with user called "AutoTesting"
   And Go to Administration, User Management
   And I should Reset disable user "AUTOUSER" to Active 
   When I Logout from current user
   And I login with user called "AUTOUSER"
   Then I should see elluminate Home page

Scenario:Elluminate_User_Lock_SmokeTest 
	Given We are at the Sign In Page
   And User account is locked  after "6" unsuccessful attempt to login user called "AUTOUSER"
   Then I should see message "Your account is locked out. Please contact eClinical helpdesk."
   Given I login with user called "AutoTesting"
   And Go to Administration, User Management
   Then I should see Account is locked for user called "AUTOUSER"
   And I should Unlock user called "AUTOUSER" to Active User 
   When I Logout from current user
   And I login with user called "AUTOUSER"
   Then I should see elluminate Home page

Scenario:Elluminate_User_LoginFailed_SmokeTest 
	Given We are at the Sign In Page
	And I login with invalid Credentials using user called "AUTOUSER"
	Then I should see message "Incorrect username and/or password."

Scenario:Elluminate_User_Profile_SmokeTest 
      Given We are at the Sign In Page
	  And I login with user called "AutoTesting"
	  When I should go to User My Profile page
	  Then I should see user information "My Information,My Preferences,My Privileges and Studies,My Studies and Blinded Sets"
	  When I Edit Phone "004567898" and Fax "780808900"
	  And I should save my Profile information
	  Then Go to User profile and Verify  Phone "004567898" and Fax "780808900" is saved
