﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Threading;
using System.Windows.Forms;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Net;
using System.IO;

namespace Automation_Elluminate
{
    public class Actiontype

    {
        // GetWebElementControl used to define By class methods eg: 'Id','Xpath','CSS','Name'
        public static By GetWebElementControl(string locator)
        {
            By by = null;
            String locatorType = null;
            String element = null;

            if (locator.StartsWith("//"))
            {
                element = locator;
                by = By.XPath(element);

            }
            else
            {
                locatorType = locator.Split('^')[0];

                element = locator.Split('^')[1];



                try
                {
                    if (locatorType.ToUpper().Equals("XPATH"))
                    {
                        by = By.XPath(element);
                    }

                    else if (locatorType.ToUpper().Equals("ID"))
                    {

                        by = By.Id(element);
                    }
                    else if (locatorType.ToUpper().Equals("NAME"))
                    {
                        by = By.Name(element);
                    }
                    else if (locatorType.ToUpper().Equals("CSS"))
                    {
                        by = By.CssSelector(element);
                    }
                    else if (locatorType.ToUpper().Equals("LINKTEXT"))
                    {
                        by = By.LinkText(element);
                    }
                    else if (locatorType.ToUpper().Equals("PARTIALTEXT"))
                    {
                        by = By.PartialLinkText(element);
                    }
                    else if (locatorType.ToUpper().Equals("TAGNAME"))
                    {
                        by = By.TagName(element);
                    }
                    else
                    {
                        throw new NoSuchElementException("Invalid Element Locator type : " + locatorType);
                    }
                }
                catch (NoSuchElementException e)
                {
                    e.ToString();
                }
            }
            

            return by;
        }

        // Click to perform click action 
        public static void click(String element)
        {
            try
            {
                ElementInfo.by = GetWebElementControl(element);
                waitforElementPresent(ElementInfo.by);
                ElementInfo.webElement.Click();
                waitForPageLoad();
                Console.WriteLine("Click action perform on element " + element);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Click action at " + element + " failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "Click action at " + element + " failed due to " + ex.Message);
                throw new mkExceptionHandler("click", ex, ex.Message);
            }
        }

        // Click to perform click action and wait for certain time 
        public static void clickAndWait(String element, String timeLimit)
        {
            try
            {
                ElementInfo.by = GetWebElementControl(element);
                waitforElementPresent(ElementInfo.by);
                ElementInfo.webElement.Click();
                Console.WriteLine("CLick action perform on element " + element);
                //TestReport.Takescreenshot("Passed", "CLick action perform on element " + element);
                Thread.Sleep(int.Parse(timeLimit));

                //((IJavaScriptExecutor)mkConstants.webDriver).ExecuteScript("var evt = document.createEvent('MouseEvents');" + "evt.initMouseEvent('click',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null);" + "arguments[0].dispatchEvent(evt);", mkConstants.webElement);

            }
            catch (ElementNotVisibleException)
            {
                for (int i = 0; i < 60; i++)
                {
                    try
                    {
                        ElementInfo.webElement.Click();
                        Console.WriteLine("CLick action perform on element " + element);
                        //TestReport.Takescreenshot("Passed", "CLick action perform on element " + element);
                        Thread.Sleep(int.Parse(timeLimit));
                        i = i + 60;
                    }
                    catch (Exception)
                    {
                        Thread.Sleep(2000);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Click action at " + element + " failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "ClickandWait action at " + element + " failed due to " + ex.Message);
                throw new mkExceptionHandler("clickAndWait", ex, ex.Message);
            }

        }

        //verify element not present click someother element again 
        public static void VerifyNotPresentAndClick(String verifyelement, String Clickelement)
        {
            try
            {
                ElementInfo.by = GetWebElementControl(verifyelement);               
                ElementInfo.webElement= ElementInfo.driver.FindElement(ElementInfo.by);
                
            }
            catch (Exception )
            {
                ElementInfo.by = GetWebElementControl(Clickelement);
                waitforElementPresent(ElementInfo.by);
                ElementInfo.webElement.Click();
                Console.WriteLine("CLick action perform on element " + Clickelement);
                TestReport.Takescreenshot("Passed", "CLick action perform on element " + Clickelement);

            }

        }

        // OPEN url value
        public static void OPenUrl()
        {
            string openurlvalue = null;

            //openurlvalue=GetHashKeyValues(Url);

            if (Startupfile.Environment.ToUpper() == "DEV")
            {
                openurlvalue = Startupfile.DevUrl;
            }
            else if (Startupfile.Environment.ToUpper() == "TESTURLC")
            {
                openurlvalue = Startupfile.TesturlC;
            }
            else if (Startupfile.Environment.ToUpper() == "TESTURLD")
            {
                openurlvalue = Startupfile.TesturlD;
            }
            try
            {
                ElementInfo.driver.Navigate().GoToUrl(openurlvalue);
                Console.WriteLine("Open Url " + openurlvalue);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Open url " + openurlvalue + " failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                throw new mkExceptionHandler("OPenUrl", ex, ex.Message);
            }
        }


        public static void loginmethod(string UserName, string Password, string ProfileName)
        {
            try
            {
               // OPenUrl();
                EnterValue(Login.Username, UserName); //"AutoTesting");
                EnterValue(Login.Password, Password); //  "Auto@403");
                clickAndWait(Login.Sumbit, "3000");
                OPenUrl();
                VerifyAttributeValue(Login.LoginPerson, "Title", ProfileName); // "SmokeTest, Auto");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Login Method failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                throw new mkExceptionHandler("loginmethod", ex, ex.Message);
            }

        }
        public static void loginmethod()
        {
            try
            {
                OPenUrl();
                EnterValue(Login.Username,"AutoTesting");
                EnterValue(Login.Password,   "Auto@403");
                clickAndWait(Login.Sumbit, "3000");
                VerifyAttributeValue(Login.LoginPerson, "Title", "SmokeTest, Auto");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Login Method failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                throw new mkExceptionHandler("loginmethod", ex, ex.Message);
            }

        }
        public static void logoutmethod()
        {
            try
            {
                click(LogOut.UserMenu);
                click(LogOut.Signoff);
                Console.WriteLine("Logout Method sucessfully verified ");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Logout Method failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                throw new mkExceptionHandler("logoutmethod", ex, ex.Message);
            }

        }

        // EnterValue at element with value
        public static void EnterValue(String element, String keyvalue)
        {
            string value = null;

            if (keyvalue.StartsWith("${"))
            {
                value = GetHashKeyValues(keyvalue);
            }
            else
            {
                 value = keyvalue;
            }

            ClearText(element);

            try
            {
                ElementInfo.by = GetWebElementControl(element);
                waitforElementPresent(ElementInfo.by);
                ElementInfo.webElement.SendKeys(value); 

                //((IJavaScriptExecutor)ElementInfo.driver).ExecuteScript("arguments[0].value=arguments[1]", ElementInfo.webElement, value);
                //ElementInfo.webElement.SendKeys("");
                //Console.WriteLine("EnterValue action perform on element " + element);
            }
            catch (Exception ex)
            {
                Console.WriteLine("EnterValue action at " + element + " failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "EnterValue action at " + element + " failed due to " + ex.Message);
                throw new mkExceptionHandler("EnterValue ", ex, ex.Message);
            }
        }

        // StoreText at element to key 
        public static void StoreText(String element, String key)
        {
            string Tempvalue = null;

            try
            {
                ElementInfo.by = GetWebElementControl(element);
                waitforElementPresent(ElementInfo.by);
                Tempvalue =ElementInfo.webElement.Text;
                Variables.DataSetTable.Add(key, Tempvalue);
            }
            catch (Exception ex)
            {
                Console.WriteLine("StoreText action at " + element + " failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "StoreText action at " + element + " failed due to " + ex.Message);
                throw new mkExceptionHandler("StoreText ", ex, ex.Message);
            }
        }

        // SelectDropdownValue at element with value
        public static void SelectDropdownValue(String element,String Attribute, String keyvalue)
        {
            string value = null;

            if (keyvalue.StartsWith("${"))
            {
                value = GetHashKeyValues(keyvalue);
            }
            else
            {
                value = keyvalue;
            }
                       

            try
            {
                ElementInfo.by = GetWebElementControl(element);
                waitforElementPresent(ElementInfo.by);

                if (Attribute.ToUpper() == "TEXT")
                {
                    var selectElement = new SelectElement(ElementInfo.webElement);
                    selectElement.SelectByText(keyvalue);
                }
                else if (Attribute.ToUpper() == "INDEX")
                {
                    var selectElement = new SelectElement(ElementInfo.webElement);
                    selectElement.SelectByIndex(int.Parse(keyvalue));
                }
                else if(Attribute.ToUpper() == "VALUE")
                {
                    var selectElement = new SelectElement(ElementInfo.webElement);
                    selectElement.SelectByValue(keyvalue);
                }
                Console.WriteLine("Select action perform on element " + element );
            }
            catch (Exception ex)
            {
                Console.WriteLine("Select action perform on element " + element + " failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "Select action perform on element " + element + " failed due to " + ex.Message);
                throw new mkExceptionHandler("Select action ", ex, ex.Message);
            }
        }

        // Clear text feild at element 
        public static void ClearText(String element)
        {            

            try
            {
                ElementInfo.by = GetWebElementControl(element);
                waitforElementPresent(ElementInfo.by);
                ElementInfo.webElement.Clear();
               Console.WriteLine("ClearText action perform on element " + element);
            }
            catch (Exception ex)
            {
                //Console.WriteLine("ClearText action at " + element + " failed due to " + ex.Message);
                //Variables.isTestCaseFailed = true;
                //TestReport.Takescreenshot("Failed", "ClearText action at " + element + " failed due to " + ex.Message);
                //throw new mkExceptionHandler("ClearText ", ex, ex.Message);
            }
        }

        //wait until for element present with max 3min if u want to increase time we can do. 
        public static void waitforElementPresent(By element)
        {
            bool isElementPresent = false;
            int i = 0;

            while (i < 120)
            {
                try
                {

                    ElementInfo.webElement = ElementInfo.driver.FindElement(element);
                    isElementPresent = true;
                    break;
                }

                catch (Exception ex)
                {
                    Thread.Sleep(1000);
                    i++;
                }
            }

            if (isElementPresent)
            {
               Console.WriteLine("waitforElementPresent function - Element is present : " + element);
            }
            else
            {
                Console.WriteLine("waitforElementPresent Element is not present : " + element);
                Variables.isTestCaseFailed = true;
                throw new NoSuchElementException("Invalid Element Locator type : " + ElementInfo.by);
            }
        }

        

        public static void VerifyElementPresent(String element)
        {
            bool isElementPresent = false;
            int i = 0;
            ElementInfo.by = GetWebElementControl(element);
            while (i < 60)
            {
                try
                {

                    ElementInfo.webElement = ElementInfo.driver.FindElement(ElementInfo.by);
                    isElementPresent = true;
                    break;
                }

                catch (Exception ex)
                {
                    Thread.Sleep(1000);
                    i++;
                }
            }

            if (isElementPresent)
            {
                Console.WriteLine("VerifyElementPresent function executed successfully : " + element);
            }
            else
            {
                Console.WriteLine("VerifyElementPresent function failed due to  : " + element);
                Variables.isTestCaseFailed = true;
                throw new NoSuchElementException("Invalid Element Locator type : " + ElementInfo.by);
            }
        }

        // VerifyElementNotPresent function it will wait for 30 sec to check the element not present , 
        // element is present after 30 sec function will get failed
        public static void VerifyElementNotPresent(String element)
        {
            bool isElementnotPresent = false;
            int i = 0;
            ElementInfo.by = GetWebElementControl(element);

            while (i < 30)
            {
                try
                {

                    ElementInfo.webElement = ElementInfo.driver.FindElement(ElementInfo.by);
                    Thread.Sleep(1000);
                    i++;
                }

                catch (Exception)
                {
                    isElementnotPresent = true;
                    break;
                }
            }

            if (isElementnotPresent)
            {
                Console.WriteLine("VerifyElementNotPresent function executed successfully : " + element);
                
            }
            else
            {
                Console.WriteLine("VerifyElementNotPresent function failed due to element is present after 30 sec : " + element);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "VerifyElementNotPresent function failed due to element is present after 30 sec : " + element);
                throw new NoSuchElementException("Invalid Element Locator type : " + ElementInfo.by);
            }
        }

        //  "time": secound- Wait for element until not status not present 
        // element still present after time - then log message element exists after this time

        public static void WaitUntilElementNotPresent(String element, int time)
        {
            bool isElementNotPresent = false;

            int i = 0;
            ElementInfo.by = GetWebElementControl(element);
            while (i < time)
            {
                try
                {

                    ElementInfo.webElement = ElementInfo.driver.FindElement(ElementInfo.by);
                    Thread.Sleep(1000);
                    i++;
                }

                catch (Exception)
                {

                    isElementNotPresent = true;
                    break;
                }
            }

            if (isElementNotPresent)
            {
               Console.WriteLine("WaitUntilElementNotPresent executed sucessfully : " + element);
            }
            else
            {
                Console.WriteLine("WaitUntilElementNotPresent failed due to element is present after waiting " + time +" secound" + element);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "WaitUntilElementNotPresent failed due to element is present after waiting " + time + " secound" + element);
                throw new NoSuchElementException("Invalid Element Locator type : " + ElementInfo.by);
            }
        }
        //waituntilelementPresent - it will wait for element in time, if time exceeds function will fail due to element present after the given time limit 
        public static void WaitUntilElementPresent(String element, int time)
        {
            bool isElementPresent = false;
            int i = 0;
            ElementInfo.by = GetWebElementControl(element);
            while (i < time)
            {
                try
                {

                    ElementInfo.webElement = ElementInfo.driver.FindElement(ElementInfo.by);
                    isElementPresent = true;
                    break;
                }

                catch (Exception)
                {
                    Thread.Sleep(1000);
                    i++;

                   
                }
            }

            if (isElementPresent)
            {
               Console.WriteLine("WaitUntilElementPresent function executed successfully : " + element);
                
            }
            else
            {
                Console.WriteLine("WaitUntilElementPresent function failed due to element present after the given time limit : " + element);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "WaitUntilElementPresent function failed due to element present after the given time limit : " + element);
                throw new NoSuchElementException("Invalid Element Locator type : " + ElementInfo.by);
            }
        }

        // click if present function - it will click only element is present , if element is not present it won't throw any expection 
        public static void ClickIfPresent(String element)
        {
            bool isElementPresent = false;
            int i = 0;
            ElementInfo.by = GetWebElementControl(element);
            while (i < 5)
            {
                try
                {

                    ElementInfo.webElement = ElementInfo.driver.FindElement(ElementInfo.by);
                    isElementPresent = true;
                    break;
                }

                catch (Exception)
                {
                    Thread.Sleep(1000);
                    i++;
                }
            }

            if (isElementPresent)
            {
                ElementInfo.driver.FindElement(ElementInfo.by).Click();
            }
            
        }

        // get attribute value base on the AttributeText
        public static String GetAttributeValue(String element, String AttributeText)
        {
            String value = null;

            try
            {
                ElementInfo.by = GetWebElementControl(element);
                waitforElementPresent(ElementInfo.by);
                value = ElementInfo.webElement.GetAttribute(AttributeText);

               Console.WriteLine("GetAttributeValue of element " + element);

            }
            catch (Exception ex)
            {
                Console.WriteLine("GetAttributeValue of " + element + " failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "GetAttributeValue of " + element + " failed due to " + ex.Message);
                throw new mkExceptionHandler("GetAttributeValue", ex, ex.Message);
            }
            return value;

        }

        // get Tex tfor an element
        public static String GetText(String element)
        {
            String value = null;

            try
            {
                ElementInfo.by = GetWebElementControl(element);
                waitforElementPresent(ElementInfo.by);
                value = ElementInfo.webElement.Text;

                Console.WriteLine("GetText of element " + element);

            }
            catch (Exception ex)
            {
                Console.WriteLine("GetText of " + element + " failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "GetText of " + element + " failed due to " + ex.Message);
                throw new mkExceptionHandler("GetText", ex, ex.Message);
            }
            return value;

        }

        private static Random random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
       
        //verify attribute value base on the input element
        public static void VerifyAttributeValue(String element, String Attribute, String keyvalue)
        {


            string value = null;

            if (keyvalue.StartsWith("${"))
            {
                value = GetHashKeyValues(keyvalue);
            }
            else
            {
                value = keyvalue;
            }

            //String value = GetHashKeyValues(valuekey);

            try
            {

                String Attributevalue = GetAttributeValue(element, Attribute);

                if (Attributevalue.ToUpper().Equals(value.ToUpper()))
                {
                    Console.WriteLine("VerifyAttributeValue on element " + element + " : Expected value =" + value + " Actual value =" + Attributevalue);
                    
                }
                else
                {
                    Console.WriteLine("VerifyAttributeValue on element " + element + " : Expected value =" + value + " Actual value =" + Attributevalue);
                    Variables.isTestCaseFailed = true;
                    TestReport.Takescreenshot("Failed" , "VerifyAttributeValue on element " + element + " : Expected value =" + value + " Actual value =" + Attributevalue);
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine("VerifyAttributeValue on " + element + " failed due to " + ex.Message);
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "VerifyAttributeValue on " + element + " failed due to " + ex.Message);
                throw new mkExceptionHandler("VerifyAttributeValue", ex, ex.Message);
            
        }

        }

        //Get hashkey value

        public static String GetHashKeyValues(String keyvalue)
        {
            keyvalue = keyvalue.Replace("${", "");
            keyvalue = keyvalue.Replace("}", "");
           // string key = keyvalue + Variables.iteration;
            keyvalue = Variables.DataSetTable[keyvalue].ToString();
            return keyvalue;
        }
        

        public static void waitForPageLoad()
        {
            Boolean isPageLoad = false;
            int time = 0;
            while (time < 300)
            {

                CheckPageState:

                try
                {

                var pageLoadState = ((IJavaScriptExecutor)ElementInfo.driver).ExecuteScript(@"if (document != undefined && document.readyState) { return document.readyState;} else { return undefined;}").ToString();

                    if (pageLoadState.Equals("complete"))
                    {
                        isPageLoad = true;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("waitForPageLoad throwing expection : " + ex.Message);
                }

                //if pageload state is true quite

                if (isPageLoad)
                {
                    Console.WriteLine("Page load state is true");

                    break;
                }
                // else wait and verify again pageload state 
                else
                {
                    time++;
                    goto CheckPageState;
                }
            }

            //if pageload state is true quite

            if (isPageLoad)
            {
                Console.WriteLine("Page load state is true");
                
            }
            // else wait and verify again pageload state 
            else
            {
                Console.WriteLine("Page load state is flase");
                Variables.isTestCaseFailed = true;
            }

        }

        public static void DemowaitForPageLoad()
        {
            Boolean isPageLoad = false;
            int time = 0;
            while (time < 300)
            {

                CheckPageState:

                try
                {
                    waitForJsLoad();

                    pageLoadTime();

                    ((IJavaScriptExecutor)ElementInfo.driver).ExecuteScript("var s=window.document.createElement('script');s.src = 'somescript.js';window.document.head.appendChild(s); ");

                    var pageLoadState = ((IJavaScriptExecutor)ElementInfo.driver).ExecuteScript(@"if (document != undefined && document.readyState) { return document.readyState;} else { return undefined;}").ToString();

                    if (pageLoadState.Equals("complete"))
                    {
                        isPageLoad = true;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("waitForPageLoad throwing expection : " + ex.Message);
                }

                //if pageload state is true quite

                if (isPageLoad)
                {
                    Console.WriteLine("Page load state is true");

                    break;
                }
                // else wait and verify again pageload state 
                else
                {
                    time++;
                    goto CheckPageState;
                }
            }

            //if pageload state is true quite

            if (isPageLoad)
            {
                Console.WriteLine("Page load state is true");

            }
            // else wait and verify again pageload state 
            else
            {
                Console.WriteLine("Page load state is flase");
                Variables.isTestCaseFailed = true;
            }

        }

        public static void waitForJsLoad()
        {
            while (true)
            {
                try
                {

                    var result = (Boolean)((IJavaScriptExecutor)ElementInfo.driver).ExecuteScript("return jQuery.active==0");
                    if (result)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    break;
                }


            }

        }

        public static void pageLoadTime()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)ElementInfo.driver;

            js.ExecuteScript("return jquery.active==0");

            // Total requests, made to get page rendered
            long totalRequests = (long)(js.ExecuteScript("return window.performance.getEntries().length;"));
            //System.out.println("Total requests: " + totalRequests);

            // Time to load DOM Content
            long domLoadEventEnd = (long)js.ExecuteScript("return window.performance.timing.domContentLoadedEventEnd;");
            long fetchStart = (long)js.ExecuteScript("return window.performance.timing.fetchStart;");
            //System.out.println("DOM Content Loaded: " + (domLoadEventEnd - fetchStart) + " ms.");

            // Time to load entire page
            long loadEventEnd = (long)js.ExecuteScript("return window.performance.timing.loadEventEnd;");
           // System.out.println("Loaded: " + (loadEventEnd - fetchStart) + " ms.");

        }

        public static void CheckAlert()
        {
            //bool isAlertNotPresent = false;
            bool isAlertPresent = false;

            //isAlertNotPresent = isElementPresent(Alerts.NotPresent); 
            
            isAlertPresent =  isElementPresent(Alerts.Present); 

            if (isAlertPresent)
            {
                Console.WriteLine("Alert present refer screenshot");
                Variables.isTestCaseFailed = true;
                TestReport.Takescreenshot("Failed", "Alert present refer screenshot");
                throw new mkExceptionHandler("Alert present refer screenshot",null,"Alert popup");

            }



        }

        public static void Uploadfile(String filepath)
        {
            try
            {

                Thread.Sleep(5000);
                SendKeys.SendWait("^a");
                Thread.Sleep(1000);
                SendKeys.SendWait(filepath);
                Thread.Sleep(1000);
                SendKeys.SendWait(@"{Enter}");
                Thread.Sleep(1000);
                TestReport.Takescreenshot("PASSED", "Uploadfile" + filepath);
                Console.WriteLine("Uploadfile done successfully");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Uploadfile failed due to " + ex.Message);
                TestReport.Takescreenshot("Failed", "Uploadfile failed due to " + ex.Message + filepath);
                throw new mkExceptionHandler("Uploadfile", ex, ex.Message);
            }
        }

        public static Boolean isElementPresent(String Element)
        {

            bool isPresent = false;

            ElementInfo.by = GetWebElementControl(Element);

            try
            {
                ElementInfo.driver.FindElement(ElementInfo.by);

                isPresent = true;

            }
            catch (Exception)
            {
                                
            }

            return isPresent;


        }

        public static void DragAndDrop(String FromElement, String ToElement)
        {
            Actions actiontype = new Actions(ElementInfo.driver);

            ElementInfo.by = GetWebElementControl(FromElement);
            IWebElement source = ElementInfo.driver.FindElement(ElementInfo.by);

            ElementInfo.by = GetWebElementControl(ToElement);
            IWebElement target = ElementInfo.driver.FindElement(ElementInfo.by);

            string dragAndDrop = System.IO.File.ReadAllText("C:\\DragAndDrop.txt"); //refer code saved in c drive with dragandDrop text file
            dragAndDrop += "simulateHTML5DragAndDrop(arguments[0], arguments[1])";
            IJavaScriptExecutor executor = (IJavaScriptExecutor)ElementInfo.driver;
            executor.ExecuteScript(dragAndDrop, source, target); 

        }
    }
}