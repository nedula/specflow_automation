﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Automation_Elluminate
{
    public class mkExceptionHandler : Exception /*  class - for handling user derfined and pre-defined exception*/
    {
        public mkExceptionHandler( String MethodName, Exception exceptionType, String errorMessage)
        {
            //NoSuchElementException
            if (exceptionType.GetType().ToString().ToUpper().Contains("OpenQA.Selenium.NoSuchElementException".ToUpper()))
            {




            }
            else if (exceptionType.GetType().ToString().ToUpper().Contains("System.Runtime.InteropServices.COMException".ToUpper()))
            {
                if (MethodName.Equals("ReadTestData"))
                {
                    Console.WriteLine("Read test data failed due to sheet name not found " + errorMessage);
                }


            }

            //WebDriverException
            else if (exceptionType.GetType().ToString().ToUpper().Contains("OpenQA.Selenium.WebDriverException".ToUpper()))
            {

            }
            //AsertFailedException
            else if (exceptionType.GetType().ToString().ToUpper().Contains("Microsoft.VisualStudio.TestTools.UnitTesting.AssertFailedException".ToUpper()))
            {

            }

            //InvalidOperationException
            else if (exceptionType.GetType().ToString().ToUpper().Contains("InvalidOperationException".ToUpper()))
            {

            }

            //WebDriver Render exception
            else if (exceptionType.GetType().ToString().ToUpper().Contains("OpenQA.Selenium.WebDriverTimeoutException".ToUpper()))
            {
            }

            //StaleElementReferenceException
            else if (exceptionType.GetType().ToString().ToUpper().Contains("OpenQA.Selenium.StaleElementReferenceException".ToUpper()))
            {

            }

            //Handling Number format exceptions
            else if (exceptionType.GetType().ToString().ToUpper().Contains("System.FormatException".ToUpper()))
            {

            }

            //mkThreadAbortExcepion - If Test exceeds the time it'll caputre screen.
            else if (exceptionType.GetType().ToString().ToUpper().Contains("ThreadAbortException".ToUpper()))
            {

            }
            //Handling Null webElements
            else if (exceptionType.GetType().ToString().ToUpper().Contains("NullReferenceException".ToUpper()))
            {

            }

            else if (exceptionType.GetType().ToString().ToUpper().Contains("OpenQA.Selenium.ElementNotVisibleException".ToUpper()))
            {

            }
            else if (exceptionType.GetType().ToString().ToUpper().Contains("OpenQA.Selenium.UnhandledAlertException".ToUpper()))
            {

            }

            else
            {

            }
        }
    }
}
