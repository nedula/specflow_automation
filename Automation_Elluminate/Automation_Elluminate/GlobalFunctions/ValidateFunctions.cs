﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using System.Threading;
using System.Data;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Support.UI;

namespace Automation_Elluminate
{
    class Functions
    {

        public static void ValidateRowExist(String table, String RowData)
        {
            int rowcount = 0;
            IList<IWebElement> TableList = ElementInfo.driver.FindElements(By.XPath(table));           

            rowcount = TableList.Count;

            bool isRowExist = false;

            for (int row = 1; row < rowcount; row++)
            {
                try
                {
                    String temptext = ElementInfo.driver.FindElement(By.XPath(table+"[" + row + "]/td[1]")).Text;

                        if (RowData.ToUpper().Trim().Equals(temptext.ToUpper().Trim()))
                        {
                        isRowExist = true;
                            row = rowcount + 1;
                        }
                                      
                }
                catch (Exception)
                {
                }
            }

            if (isRowExist)
            {
                Console.WriteLine("ValidateRowExist is executed sucessfully");
            }
            else
            {
                Console.WriteLine("ValidateRowExist is failed due to Data not exists in Table");
                Variables.isTestCaseFailed = true;
                Report.ErrorMessage = "ValidateRowExist is failed due to Data not exists in Table";
                TestReport.Takescreenshot("Failed", "ValidateRowExist is failed due to " + RowData + "Data not exists in Table");
                throw new mkExceptionHandler("ValidateRowExist ", null, "ValidateRowExist is failed due to " + RowData + "Data not exists in Table");
            }
        }

        public static void ValidateRowNotExist(String table, String RowData)
        {

            IList<IWebElement> TableList = ElementInfo.driver.FindElements(By.XPath(table));
            int rowcount = TableList.Count;

            bool isRowExist = false;

            for (int row = 1; row < rowcount; row++)
            {
                try
                {
                    String temptext = ElementInfo.driver.FindElement(By.XPath(table+"[" + row + "]/td[1]")).Text;

                    if (RowData.ToUpper().Trim().Equals(temptext.ToUpper().Trim()))
                    {
                        isRowExist = true;
                        row = rowcount + 1;
                    }
                }
                catch (Exception)
                {
                }
            }

            if (!isRowExist)
            {
                Console.WriteLine("ValidateRowNotExist is executed sucessfully");
            }
            else
            {
                Console.WriteLine("ValidateRowNotExist is failed due to data exists in a table");
                Variables.isTestCaseFailed = true;
                Report.ErrorMessage = "ValidateRowNotExist is failed due to data  exists in a table";
                TestReport.Takescreenshot("Failed", "ValidateRowNotExist is failed due to " + RowData + "data exists in a table");
                throw new mkExceptionHandler("ValidateRowNotExist ", null, "ValidateRowNotExist is failed due to " + RowData + " data exists in a table");
            }
        }

        public static void EditROw(String table, String Name, String NewName)
        {
            IList<IWebElement> TableList = ElementInfo.driver.FindElements(By.XPath(table));
            int rowcount = TableList.Count;

            bool isRowxist = false;

            for (int row = 1; row < rowcount; row++)
            {
                try
                {
                    String temptext = ElementInfo.driver.FindElement(By.XPath(table+"[" + row + "]/td[1]")).Text;

                    if (Name.ToUpper().Trim().Equals(temptext.ToUpper().Trim()))
                    {
                        isRowxist = true;
                        if (table.ToUpper().Contains("COMPOUND"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]/td[3]")).Click();
                            Actiontype.EnterValue(Compounds.CompoundName, NewName);
                            Actiontype.EnterValue(Compounds.CompoundDiscription, NewName);
                            Actiontype.clickAndWait(Compounds.Save, "2000");
                            Functions.ValidateRowExist(Compounds.CompoundTable, NewName);
                        }
                        else if (table.ToUpper().Contains("STAGING"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]/td[6]")).Click();
                            Actiontype.EnterValue( StagingAreas.Name, NewName);
                            Actiontype.EnterValue(StagingAreas.Discription, NewName);
                            Actiontype.clickAndWait(StagingAreas.Save, "2000");
                            Functions.ValidateRowExist(StagingAreas.StagingAreaTable, NewName);
                        }

                        else if (table.ToUpper().Contains("DATAMART"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]/td[6]")).Click();
                            Actiontype.EnterValue(DataMarts.Name, NewName);
                            Actiontype.EnterValue(DataMarts.Discription, NewName);
                            Actiontype.clickAndWait(DataMarts.Save, "2000");
                            Functions.ValidateRowExist(DataMarts.DataMartsTable, NewName);
                        }

                        row = rowcount + 1;
                    }
                }
                catch (Exception)
                {
                }
            }

            if (isRowxist)
            {
                Console.WriteLine("EditROw is executed sucessfully");
            }
            else
            {
                Console.WriteLine("EditROw is failed due to Data not exists in page");
                Variables.isTestCaseFailed = true;
                Report.ErrorMessage = "EditROw is failed due to " + Name + " data not exists in Table";
                TestReport.Takescreenshot("Failed", "EditROw is failed due to " + Name + " data not exists in Table");
                throw new mkExceptionHandler("EditROw ", null, "EditROw is failed due to " + Name + " data not exists in Table");
            }

        }

        public static void DeleteRow(String table, String RowData)
        {
            IList<IWebElement> TableList = ElementInfo.driver.FindElements(By.XPath(table));
            int rowcount = TableList.Count;

            bool isRowExist = false;

            for (int row = 1; row < rowcount; row++)
            {
                try
                {
                    String temptext = ElementInfo.driver.FindElement(By.XPath(table+"[" + row + "]/td[1]")).Text;

                    if (RowData.ToUpper().Trim().Equals(temptext.ToUpper().Trim()))
                    {
                        isRowExist = true;
                        if (table.ToUpper().Contains("COMPOUND"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table+"[" + row + "]/td[4]")).Click();
                        }
                        else if (table.ToUpper().Contains("STAGING"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]/td[7]")).Click();
                        }
                        else if (table.ToUpper().Contains("DATAMART"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]/td[7]")).Click();
                        }
                            
                        Actiontype.clickAndWait(Compounds.DeleteButton, "2000");
                        row = rowcount + 1;

                    }
                }
                catch (Exception)
                {
                }
            }

            if (isRowExist)
            {
                Console.WriteLine("DeleteRow is executed sucessfully");
            }
            else
            {
                Console.WriteLine("DeleteRow is failed due to data not exists in Table");
                Variables.isTestCaseFailed = true;
                Report.ErrorMessage = "DeleteRow is failed due to data not exists in Table";
                TestReport.Takescreenshot("Failed", "DeleteRow is failed due to data not exists in Table");
            }
        }

        public static void UserActions(String table, String ActionType, String UserName, Table Userinfo )
        {
            IList<IWebElement> TableList = ElementInfo.driver.FindElements(By.XPath(table));

            int rowcount = TableList.Count;

            bool isRowxist = false;

            for (int row = 1; row < rowcount; row++)
            {
                try
                {
                    String temptext = ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]/td[1]")).Text;

                    if (UserName.ToUpper().Trim().Contains(temptext.ToUpper().Trim()) || temptext.ToUpper().Trim().Contains(UserName.ToUpper().Trim()))
                    {
                        isRowxist = true;

                        if (ActionType.ToUpper().Contains("VERIFYUSER"))
                        {
                            Console.WriteLine(UserName , "is exists in the user list");
                        }

                        else if (ActionType.ToUpper().Contains("DELETE"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]//div[text()='delete']")).Click();                           
                            Actiontype.clickAndWait(Users.Confirm, "2000");
                            
                            Console.WriteLine(UserName, "is Deleted from the user list");
                        }
                        else if (ActionType.ToUpper().Contains("EDIT"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]//div[text()='mode_edit']")).Click();
                            
                            Actiontype.EnterValue(Users.FirstName, Userinfo.Rows[0][1].ToString());
                            Actiontype.EnterValue(Users.Company, Userinfo.Rows[0][2].ToString());
                            Actiontype.EnterValue(Users.LastName, Userinfo.Rows[0][3].ToString());
                            Actiontype.EnterValue(Users.Email, Userinfo.Rows[0][4].ToString());
                            Actiontype.clickAndWait(Users.UserSavebtn, "3000");
                            
                            Console.WriteLine(UserName + "is Edited from the user list to"+ Userinfo.Rows[0][0].ToString());
                        }

                        else if (ActionType.ToUpper().Contains("DISABLE"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]//div[@title='Disable']")).Click();
                            Actiontype.clickAndWait(Users.Confirm, "2000");
                            Console.WriteLine(UserName + "is DISABLE from the user list ");
                            
                        }
                        else if (ActionType.ToUpper().Contains("UNLOCK"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]//div[@title='Unlock']")).Click();
                            Actiontype.clickAndWait(Users.Confirm, "2000");
                            Console.WriteLine(UserName + "is UNLOCK from the user list ");
                        }
                        else if (ActionType.ToUpper().Contains("LOCKED"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]//div[@title='Unlock']"));
                           
                            Console.WriteLine(UserName + "is LOCKed from the user list ");
                        }
                        else if (ActionType.ToUpper().Contains("ENABLE"))
                        {
                            ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]//div[@title='Enable']")).Click(); 
                            Actiontype.clickAndWait(Users.Confirm, "2000");
                            Console.WriteLine(UserName + "is ENABLE from the user list ");
                        }

                        row = rowcount + 1;
                    }
                }
                catch (Exception)
                {
                }
            }

            if (isRowxist)
            {
                Console.WriteLine("UserActions is executed sucessfully for action type" + ActionType );
            }
            else
            {
                if (ActionType.ToUpper().Contains("VERIFYUSERNOTEXIST"))
                {
                    Console.WriteLine(UserName, "is not exists in the user list");
                }
                else
                {
                    Console.WriteLine("UserActions is failed due to Data not exists in page");
                    Variables.isTestCaseFailed = true;
                    Report.ErrorMessage = "UserActions is failed for action type" + ActionType + "And" + UserName + " not exists in Table";
                    TestReport.Takescreenshot("Failed", "UserActions is failed for action type" + ActionType + "And" + UserName + " not exists in Table");
                    throw new mkExceptionHandler("UserActions ", null, "UserActions is failed for action type" + ActionType + "And" + UserName + " not exists in Table");
                }
            }

        }

        public static void DeleteSource()
        {

            try
            {

                DeleteData:

                IList<IWebElement> displayedOptions = ElementInfo.driver.FindElements(By.XPath("//table[@class='table table-hover table-show-selected']/tbody/tr"));

                int rowcount = displayedOptions.Count;

                Thread.Sleep(10000);
                for (int i = 0; i < rowcount; i++)
                {
                    try
                    {
                        Actiontype.ClickIfPresent(DataSources.DSCheckbox);
                    }
                    catch (Exception)
                    { }
                    Actiontype.clickAndWait(DataSources.DSDeletebutton, "2000");
                    Actiontype.clickAndWait(DataSources.DSYesbutton, "2000");


                }

                // verifying data is deleted or not 

                displayedOptions = ElementInfo.driver.FindElements(By.XPath("//table[@class='table table-hover table-show-selected']/tbody/tr"));

                rowcount = displayedOptions.Count;

                if (rowcount == 0)
                {

                    Console.WriteLine("DeleteDataSource method executed sucuessfully");
                }
                else
                {

                    goto DeleteData;
                }
            }
            catch (Exception ex)
            {
                Variables.isTestCaseFailed = true;
                Console.WriteLine("Test case failed due to DeleteDataSource method " + ex.Message);
                throw new mkExceptionHandler("DeleteDataSource", ex, ex.Message);
            }
        }

        public static void VerifyImportHistory()
        {
            try
            {
                IList<IWebElement> displayedOptions = ElementInfo.driver.FindElements(By.XPath("//div[@class='tab-pane ng-scope active']//table[@class='table table-hover table-show-selected']/tbody/tr"));

                int rowcount = displayedOptions.Count;

                ElementInfo.driver.FindElement(By.XPath("//tr[1]/td[4]/span[contains(text(), 'Complete')]"));
                //ElementInfo.driver.FindElement(By.XPath("//tbody/tr[1]/td[7][contains(text(), '" + DateTime.Now.ToString("dd MMM yyyy") + "')]"));

                if (rowcount > 0)
                {

                    Console.WriteLine("VerifyImportHistory method executed sucuessfully");
                }
            }
            catch (Exception ex)
            {
                Variables.isTestCaseFailed = true;
                Console.WriteLine("Test case failed due to VerifyImportHistorymethod" + ex.Message);
                throw new mkExceptionHandler("VerifyImportHistory", ex, ex.Message);
            }
        }

        public static void DeleteImportHistory()
        {
            IList<IWebElement> displayedOptions = ElementInfo.driver.FindElements(By.XPath("//div[@class='tab-pane ng-scope active']//table[@class='table table-hover table-show-selected']/tbody/tr"));

            int rowcount = displayedOptions.Count;
            int i = 0;
            try
            {

                for (i = 1; i < rowcount; i++)
                {
                    ElementInfo.driver.FindElement(By.XPath("//div[@class='tab-pane ng-scope active']//table[@class='table table-hover table-show-selected']/tbody/tr[" + i + "]/td[1]/span")).Click();
                    ElementInfo.driver.FindElement(By.XPath("//tr[" + i + "]/td[4]/span[contains(text(), 'Ready')]"));
                    Actiontype.clickAndWait(ImportFile.HistoryDelete, "2000");
                    Actiontype.clickAndWait(DataSources.DSYesbutton, "1000");
                }

            }
            catch (Exception ex)
            {
                if (rowcount > 0)
                {

                    Console.WriteLine("DeleteImportHistory method executed sucuessfully");
                }
                Console.WriteLine("DeleteImportHistory method executed sucuessfully no ready state exist");
            }
        }

        public static void VeirfyScheduleTask()
        {

            Actiontype.clickAndWait(ImportFile.Task, "5000");

            TestReport.Takescreenshot("PASSED", "Task schedule verification page");

            Actiontype.VerifyElementPresent(ImportFile.TaskscheduleCount);
            //get before schedule task count
            IList<IWebElement> displayedOptions = ElementInfo.driver.FindElements(By.XPath("//div[@class='t-grid-content']/table/tbody/tr"));
            int beforecount = displayedOptions.Count;

            int i = 1;
            //verifying schedule task updated in the task list or not 
            while (i < beforecount)
            {
                String temp = ElementInfo.driver.FindElement(By.XPath("//div[@class='t-grid-content']/table/tbody/tr[" + i + "]/td[2]")).Text;

                if (Variables.RandomString.Contains(temp) || temp.Contains(Variables.RandomString))
                {
                    Actiontype.VerifyElementPresent("XPATH^//div[@class='t-grid-content']/table/tbody/tr[" + i + "]/td[4][text()='Never']");

                    break;
                }
                else
                {
                    i++;
                }

            }

            // wait for 2 min 
            for (int wait = 0; wait < 12; wait++)
            {
                Actiontype.clickAndWait(ImportFile.Task, "2000");
                Thread.Sleep(10000);
            }

            i = 1;
            //verifying schedule task updated in the task list or not 
            while (i < beforecount)
            {
                String temp = ElementInfo.driver.FindElement(By.XPath("//div[@class='t-grid-content']/table/tbody/tr[" + i + "]/td[2]")).Text;
                if (Variables.RandomString.Contains(temp) || temp.Contains(Variables.RandomString))
                {
                    Actiontype.WaitUntilElementPresent("XPATH^//div[@class='t-grid-content']/table/tbody/tr[" + i + "]/td[5][text()='Succeeded']", 600);
                    TestReport.Takescreenshot("PASSED", "Task schedule for " + Variables.RandomString + "completed ");
                    break;
                }
                else
                {
                    i++;
                }

            }
        }

        public static void VerifyExportProcess(String Area, string Format, string ProcessStatus)
        {
            // verify export status is complete or not
            int wait = 0;

            try
            {

                while (wait < 800)
                {
                    String status = ElementInfo.driver.FindElement(By.XPath("//div[@class='col-md-12 well']/div[1]")).Text;
                    //Name: XLSX Data Store: Clinical Created By: AUTOTESTING, a few seconds ago Status: Completed\r\nDownload

                    if (Variables.currentScenarioName.Contains("Schedule"))
                    {
                        // if it is schedule test case wait for 1 min 
                        Thread.Sleep(30000);
                        status = ElementInfo.driver.FindElement(By.XPath("//div[@class='col-md-12 well']/div[1]")).Text;
                        Thread.Sleep(30000);
                        status = ElementInfo.driver.FindElement(By.XPath("//div[@class='col-md-12 well']/div[1]")).Text;

                    }

                    if (status.ToUpper().Contains(ProcessStatus.ToUpper()) && status.ToUpper().Contains(Area.ToUpper()) && status.ToUpper().Contains(Format.ToUpper()))
                    {
                        Console.WriteLine("Export status is complete successfully");
                        TestReport.Takescreenshot("PASSED", "Export status is complete successfully");
                        break;
                    }
                    else
                    {
                        if (status.ToUpper().Contains("ERROR"))
                        {
                            TestReport.Takescreenshot("FAILED", "Export process is throwing error");
                            Variables.isTestCaseFailed = true;
                            throw new mkExceptionHandler("VerifyExportProcess ", null, "VerifyExportProcess failed due to Staus is Error");

                        }
                        wait++;
                        Thread.Sleep(2000);
                        if (Variables.currentScenarioName.Contains("Schedule"))
                        {
                            Actiontype.clickAndWait(ExportFile.ExportHeader, "5000");
                        }
                    }

                }
            }
            catch (Exception)
            {
                Console.WriteLine("VerifyExportProcess failed due to Staus is Error");
                Variables.isTestCaseFailed = true;
                Report.ErrorMessage = "VerifyExportProcess failed due to Staus is Error";
                TestReport.Takescreenshot("Failed", "VerifyExportProcess failed due to Staus is Error");
                throw new mkExceptionHandler("VerifyExportProcess ", null, "VerifyExportProcess failed due to Staus is Error");
            }

        }

        public static void ValidateDefineXMLResult(String table, Boolean value)
        {
            IList<IWebElement> TableList = ElementInfo.driver.FindElements(By.XPath(table));
            int Totalrowcount = TableList.Count;

            int ValidationRows;
            String Actualvalue = null;

            try
            {
                if (Totalrowcount > 0)
                {
                    Console.WriteLine("New row added in to the table grid: " + Totalrowcount);
                    IList<IWebElement> ValidationList = ElementInfo.driver.FindElements(By.XPath(DefineXML.Validation_Result));
                    ValidationRows = ValidationList.Count;

                    if (value.Equals(true))
                    {
                        Actualvalue = Actiontype.GetText(DefineXML.Validation_Result);
                        if (Actualvalue.Trim().Contains("validation"))
                        {

                            Console.WriteLine("New rows having validation results are added in to the table grid: " + ValidationRows);
                        }
                    }

                    if (value.Equals(false))
                    {
                        Actualvalue = Actiontype.GetText(DefineXML.TableGrid);
                        int NoValidationRows = Totalrowcount - ValidationRows;
                        Console.WriteLine("New rows without validation results are added in to the table grid: " + NoValidationRows);

                    }
                }
                else
                {
                    Console.WriteLine("No new data added in to the table grid");

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Test case failed due to ValidateDefineXMLResult method " + ex.Message);
                Variables.isTestCaseFailed = true;
                Report.ErrorMessage = "ValidateDefineXMLResult is failed due to Data not exists in Table";
                TestReport.Takescreenshot("Failed", "ValidateDefineXMLResult is failed due to " + Totalrowcount + "Data not exists in Table");
                throw new mkExceptionHandler("ValidateDefineXMLResult ", null, "ValidateDefineXMLResult is failed due to " + Totalrowcount + "Data not exists in Table");
            }
        }

        public static void GetRows(String table)
        {
            IList<IWebElement> TableList = ElementInfo.driver.FindElements(By.XPath(table));
            int Totalrowcount = TableList.Count;
            try
            {
                if (Totalrowcount > 0)
                {
                    Console.WriteLine("New row added in to the table grid: " + Totalrowcount);
                }
                else
                {
                    Console.WriteLine("No new data added in to the table grid");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Test case failed due to GetRows method " + ex.Message);
                Variables.isTestCaseFailed = true;
                Report.ErrorMessage = "GetRows is failed due to Data not exists in Table";
                TestReport.Takescreenshot("Failed", "GetRows is failed due to " + Totalrowcount + "Data not exists in Table");
                throw new mkExceptionHandler("GetRows", null, "GetRows is failed due to " + Totalrowcount + "Data not exists in Table");
            }
        }


        public static void DelDataQualityJob()
        {
            try
            {
                DeleteData:

                Actiontype.clickAndWait(DataQuality.DQListView, "2000");
                IList<IWebElement> TableList = ElementInfo.driver.FindElements(By.XPath("//table[@class='table table-striped table-hover table-show-selected']/tbody/tr"));
                int Totalrowcount = TableList.Count;
                Thread.Sleep(10000);

                try
                {
                    if (Totalrowcount > 0)
                    {
                        for (int i = 0; i < Totalrowcount; i++)
                        {
                            Actiontype.clickAndWait(DataQuality.DQDelListView, "2000");
                            Actiontype.clickAndWait(DataQuality.DQDelConfirm, "2000");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No new data added in to the table grid");
                    }
                }
                catch (Exception ex)
                { }
                // verifying data is deleted or not
                TableList = ElementInfo.driver.FindElements(By.XPath("//table[@class='table table-striped table-hover table-show-selected']/tbody/tr"));

                Totalrowcount = TableList.Count;

                if (Totalrowcount == 0)
                {
                    Console.WriteLine("DelDataQualityJob method executed sucuessfully");
                }
                else
                {
                    goto DeleteData;
                }
            }
            catch (Exception ex)
            {
                Variables.isTestCaseFailed = true;
                Console.WriteLine("Test case failed due to DelDataQualityJob method " + ex.Message);
                throw new mkExceptionHandler("DelDataQualityJob", ex, ex.Message);
            }
        }
        public static void ValidationFilter(String table)
        {
            try
            {
                IList<IWebElement> TableList = ElementInfo.driver.FindElements(By.XPath(table));
                int Totalrowcount = TableList.Count;
                Thread.Sleep(10000);
                int Err = 0;
                int Warn = 0;
                int Info = 0;
                try
                {
                    if (Totalrowcount > 0)
                    {
                        for (int row = 1; row < Totalrowcount; row++)
                        {
                            String Text = ElementInfo.driver.FindElement(By.XPath(table + "[" + row + "]/td[3]")).Text;
                            if (Text.Equals("Error"))
                            {
                                Err++;
                            }
                            else if (Text.Equals("Warning"))
                            {
                                Warn++;
                            }
                            else if (Text.Equals("Information"))
                            {
                                Info++;
                            }
                        }
                        Console.WriteLine("Total Rows with Error: " + Err);
                        Console.WriteLine("Total Rows with Warning: " + Warn);
                        Console.WriteLine("Total Rows with Information: " + Info);
                    }
                    else
                    {
                        Console.WriteLine("No new data found in to the table grid");
                    }
                }
                catch (Exception ex)
                { }
                // verify count for each filter

                /*    if (Err > 0)
                    {
                        Console.WriteLine("Total Rows with Error: " +Err);
                    }
                    else if (Warn > 0)
                    {
                        Console.WriteLine("Total Rows with Warning: " +Warn);
                    }
                    else if (Info > 0)
                    {
                        Console.WriteLine("Total Rows with Information: " +Info);
                    } */

            }
            catch (Exception ex)
            {
                Variables.isTestCaseFailed = true;
                Console.WriteLine("Test case failed due to ValidationFilter method " + ex.Message);
                throw new mkExceptionHandler("ValidationFilter", ex, ex.Message);
            }
        }
    
}
}
