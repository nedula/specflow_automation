﻿using System;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;
using RelevantCodes.ExtentReports;

namespace Automation_Elluminate
{
    class DataDriven 
    {
        public void TestExecution(string methodName)
        {
            //TestProcedure obj = new TestProcedure();
            //Type thistype = obj.GetType();
           // MethodInfo themethod = thistype.GetMethod(methodName);            

            for (Variables.iteration = 1; Variables.iteration <= Variables.iterationCount; Variables.iteration++)
            {
                try
                {
                    Console.WriteLine("Iteration" + Variables.iteration + "Started");
                    Console.WriteLine("===========================================");
                   // themethod.Invoke(this, null);
                    Console.WriteLine("===========================================");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Iteration" + Variables.iteration + "Failed due to " + ex.Message);
                }
            }
        }
    }
}
