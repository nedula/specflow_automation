﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
   
        class ImportFile
        {
            public static string ImportHeader = "XPATH^//a[@data-name='importer']";
            public static string Run = "ID^importdefinition-run";
            public static string Schedule = "ID^importdefinition-schedule";
            public static string Edit = "ID^importdefinition-edit";
            public static string Delete = "ID^importdefinition-delete";
            public static string New = "ID^importdefinition-new";
            public static string Header = "XPATH^//h4";
            //FTP 
            public static string Name = "XPATH^//input[@ng-model='importDefinition.Name']";
            public static string DataSource = "XPATH^//div[@placeholder='Select a Data Source']//i[@class='caret pull-right']";
            public static string DataSourceType = "XPATH^//Input[@placeholder='Select a Data Source']";
            public static string DataStore = "XPATH^//div[@placeholder='Select a Data Store']//i[@class='caret pull-right']";
            public static string DataStoreType = "XPATH^//Input[@placeholder='Select a Data Store']";
            public static string SASMapping = "XPATH^//div[@placeholder='Select a SAS Mapping']//i[@class='caret pull-right']";
            public static string SASMappingType = "XPATH^//Input[@placeholder='Select a SAS Mapping']";
            public static string Password = "ID^importsource-password";
            public static string ZipPassword = "ID^importdefinition-zip-file-folder";
            public static string Save = "ID^importdefinition-save";
            public static string Cancel = "ID^importdefinition-cancel";
            public static string DSChoice = "XPATH^//span[@class='ui-select-choices-row-inner']";

            public static string Integrate = "XPATH^//ui-layout-container[@id='import-tab-bottom-container']//button[@title='Integrate']";
            public static string ReadyToIntegrate = "XPATH^//ui-layout-container[@id='import-tab-bottom-container']//Span[text()='Ready To Integrate']";
            public static string Downloading = "XPATH^//ui-layout-container[@id='import-tab-bottom-container']//Span[text()='Downloading Files']";
            public static string Uploadingtostaging = "XPATH^//ui-layout-container[@id='import-tab-bottom-container']//Span[text()='Uploading To Staging Area']";
            public static string ArchiveFile = "XPATH^//ui-layout-container[@id='import-tab-bottom-container']//Span[text()='Archiving Files']";
            public static string IntegrateComplete = "XPATH^//ui-layout-container[@id='import-tab-bottom-container']//Span[text()='Complete']";
            public static string IntegrateError = "XPATH^//ui-layout-container[@id='import-tab-bottom-container']//Span[text()='Error']";


            public static string HistoryTab = "XPATH^//ul[@class='nav nav-tabs']/li[2]//a";
            public static string HistoryDelete = "XPATH^//div[@id='importrundetails-sidebar']//button[@id='importrun-deleteactive']";
            //FTP OutBound
            //importsource-remotefoldername
            //Mediro Web Service     
            //Merge eCLinicalOS Web Services  
            //Rave BioStats Gateway 
            //Rave ODM Adaptor 

            public static string Audits = "XPATH^//input[@value='Audits']";
            //Rave Web Services
            public static string DataSourceDeatils = "XPATH^//a[text()='Data Source Details']";
            public static string Incremental = "XPATH^//input[@ng-model='importDefinition.Incremental']";
            public static string IncludeLabView = "XPATH^//input[@ng-model='importDefinition.IncludeLabView']";
            public static string metadataversiondropdown = "XPATH^//div[@id='metadata-version-select']//span/i";
            public static string metadataversion = "XPATH^//div[@id='metadata-version-select']//div[3]/span";

            //Manual file import
            public static string manualFileImport = "ID^run-new-file-import";
            public static string filetype = "XPATH^//div[@placeholder='Select a File Type']";
            public static string filetypeExcel = "XPATH^//span[text()='Excel File']";
            public static string Fileupload = "ID^file";

            //schedule

            public static string Schedulebutton = "ID^wsimport-schedule";
            public static string ScheduleNext = "ID^taskwizard-next";
            public static string ScheduleTime = "ID^StartDateTime";
            public static string ScheduleClose = "ID^taskwizard-close";

            public static string Task = "XPATH^//a[@data-name='tasks']";

            public static string TaskscheduleCount = "XPATH^//div[@class='t-grid-content']/table/tbody/tr";
        }
    }
