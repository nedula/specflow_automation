﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
    
    class Users
    {
        public static string UserManagement = "LinkText^User Management";
        public static string AddUser = "XPATH^//a[text()='Add User']";
        public static string UserName = "XPATH^//li/input[contains(@id,'Login')]";
        public static string FirstName = "Id^FirstName";
        public static string Company = "Id^Company";
        public static string LastName = "Id^LastName";
        public static string Email = "Id^Email";
        public static string AdministerCheckbox = "XPATH^//table/tbody/tr[1]/td/input[@type='checkbox']";
        public static string UserSavebtn = "Id^admin-submit";
        public static string VerifyUser = "XPATH^//div/table/tbody//td[3][contains(text(),'EMAIL')]";
        public static string DeleteUser = "XPATH^//div/table/tbody//td[3][contains(text(),'EMAIL')]/following-sibling:: td[7]";
        public static string DisableUser = "XPATH^//div/table/tbody//td[3][contains(text(),'EMAIL')]/following-sibling:: td[3]";
        public static string EditUser = "XPATH^//div/table/tbody//td[3][contains(text(),'EMAIL')]/following-sibling:: td[5]";
        public static string Confirm = "ID^confirmation-submit";
        public static string UserTable = "//*[@id='users-grid']/div[3]/table/tbody/tr";

    }

    class MyProfile
    {
        public static string Profile = "XPATH^//a[text()='My Profile']";
        public static string PhoneNumber = "ID^Phone";
        public static string Fax = "ID^Fax";
        public static string save = "XPath^//input[@value='Save']";
    }
   
    
}
