﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
    class EDrive
    {
        public static string Verify_eDriveHomePage = "XPATH^//span[@id='Folder']";

        //Folder WebElements
        public static string CreateFolder = "XPATH^//span[@class='cse-folder-actions']/a[text()='Create']";
        public static string FolderName = "XPATH^//input[@name='FolderName']";
        public static string UseVersionControl = "XPATH^//input[@name='IsVersioned']";
        public static string CreateFolderSubmit = "XPATH^//input[@id='foldereditor-submit']";

        public static string RenameFolder = "XPATH^//span[@class='cse-folder-actions']/a[text()='Rename']";
        public static string RenameFolderConfirm = "ID^foldereditor-submit";

        public static string DeleteFolder = "XPATH^//span[@class='cse-folder-actions']/a[text()='Delete']";
        public static string DeleteFolderConfirm = "XPATH^//div[@class='modal-content']/div/input[@value='Delete']";
        public static string DeleteFolderCancel = "XPATH^//input[@id='folder-delete-cancel']";

        public static string UploadToFolder = "XPATH^//span[@class='cse-folder-actions']/a[text()='Upload']";
        public static string ChooseFile = "XPATH^//div[@class='t-button t-upload-button btn btn-primary pull-right margin-right-md']";
        public static string File = @"C:\Users\pchalkapure\Desktop\TEST DATA\CM.xlsx";
        public static string Comments = "XPATH^//textarea[@name='VersionComments']";
        public static string UploadConfirm = "ID^uploadfile-upload";
        public static string UploadCancel = "ID^uploadfile-cancel";
        public static string Verify_FileUploaded = "XPATH^//span[text()='uploaded']";
        public static string UploadClose = "XPATH^//input[@value='Close']";
        public static string Verify_Version = "XPATH^//table/tbody/tr/td[text()='1']";

        public static string Click_Folder = "XPATH^//span[@class='t-in' and text()='Foldername']";
        public static string Clk_tIcon = "XPATH^//span[@class='t-in' and text()='Foldername']/preceding-sibling::span";

        public static string Verify_StudyFolder = "XPATH^//span[text()='StudyFolder']";
        public static string Verify_Study1 = "XPATH^//span[text()='Study1']";

        //File WebElements

        public static string CopyFile = "XPATH^//span[@class='cse-file-actions']/a[text()='Copy']";
        public static string Click_FolderTree = "XPATH^//div[@id='folder-selector-tree']//span[text()='Foldername']";
        public static string CopyConfirm = "ID^file-copy-move-submit";
        public static string CopyCancel = "XPATH^//input[@id='file-copy-move-cancel']";
        public static string Copy_OK_btn = "XPATH^//input[@value='OK']";
        public static string Copy_Cancel_btn = "XPATH^//div[@style='text-align:right']/input[@value='Cancel']";
        public static string Close_btn = "XPATH^//button[text()='×']";


        public static string MoveFile = "XPATH^//span[@class='cse-file-actions']/a[text()='Move']";
        public static string MoveConfirm = "ID^file-copy-move-submit";
        public static string MoveCancel = "ID^file-copy-move-cancel";
        public static string Move_OK_btn = "XPATH^//input[@value='OK']";
        public static string Move_Cancel_btn = "XPATH^//div[@style='text-align:right']/input[@value='Cancel']";

        public static string DownloadFile = "XPATH^//span[@class='cse-file-actions']/a[text()='Download']";

        public static string RenameFile = "XPATH^//span[@class='cse-file-actions']/a[text()='Rename']";
        public static string FileName = "XPATH^//input[@name='FileName']";
        public static string RenameFileConfirm = "XPATH^//input[@id='file-createrename-submit']";
        public static string Verify_FileRenamed = "XPATH^//td[text()='domain.xlsx']";

        public static string DeleteFile = "XPATH^//span[@class='cse-file-actions']/a[text()='Delete']";
        public static string DeleteFileConfirm = "ID^file-delete-submit";
        public static string DeleteFileCancel = "ID^file-delete-cancel";

        public static string CheckInFile = "XPATH^//span[@class='cse-file-actions']/a[text()='Check In']";
        public static string CheckInConfirm = "XPATH^//div[@class='fresh-tilled-soil fts-temp-modal-footer modal-footer']/input[@value='Check In']";
        public static string CheckInCancel = "XPATH^//div[@class='fresh-tilled-soil fts-temp-modal-footer modal-footer']/input[@value='Cancel']";
        public static string Verify_VersionUpdated = "XPATH^//table/tbody/tr/td[text()='2']";

        public static string CheckOutFile = "XPATH^//span[@class='cse-file-actions']/a[text()='Check Out']";
        public static string CheckOutConfirm = "XPATH^//div[@class='fresh-tilled-soil fts-temp-modal-footer modal-footer']/input[@value='Check Out']";
        public static string CheckOutCancel = "XPATH^//div[@class='fresh-tilled-soil fts-temp-modal-footer modal-footer']/input[@value='Cancel']";
        public static string Verify_CheckedOutTo = "XPATH^//td[text()='AUTOTESTING']";
        public static string UndoCheckOut = "XPATH^//span[@class='cse-file-actions']/a[text()='Undo Check Out']";
        public static string UndoCheckOutConfirm = "XPATH^//div[@class='fresh-tilled-soil fts-temp-modal-footer modal-footer']/input[@value='Undo Check Out']";
        public static string UndoCheckOutCancel = "XPATH^//div[@class='fresh-tilled-soil fts-temp-modal-footer modal-footer']/input[@value='Cancel']";

        public static string SelectCheckbox = "XPATH^//div/div[@class='t-grid-content']/table/tbody/tr[1]/td/input[@type='checkbox']";
        public static string Click_Global = "XPATH^//ul[@class='t-group t-treeview-lines']/li[1]/div/span[2]";
        public static string Click_MapperTest = "XPATH^//ul[@class='t-group t-treeview-lines']/li[2]/div/span[2]";

    }
}
