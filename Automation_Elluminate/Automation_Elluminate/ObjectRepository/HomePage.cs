﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
    class ElluminateHome
    {
        public static string StudyHome = "XPATH^//a[@title='Home']";
        public static string RecentStudies = "XPATH^//li[@title='Recent Studies']/a";
        public static string DMHub = "XPATH^//a[text()='DM Hub']";
        public static string Metadata = "XPATH^//a[text()='Metadata']";
        public static string Standards = "XPATH^//a[text()='Standards']";
        public static string RBM = "XPATH^//a[text()='RBM']";
        public static string Analytics = "XPATH^//span[text()='Analytics']";
        public static string eDrive = "XPATH^//a[text()='eDrive']";
        public static string eForms = "XPATH^//a[text()='eForms']";
        public static string Administration = "XPATH^//li[@class='dropdown']/a/span[@class='caret']";
        public static string Administation_Configuration = "XPATH^//a[text()='Configuration']";
        public static string Administation_UserManagement = "XPATH^//a[text()='User Management']";
        public static string Administation_Tasks = "XPATH^ //a[text()='Tasks']";
        public static string Administation_AuditLogs = "XPATH^//a[text()='Audit Logs']";
        public static string Administation_Analytics = "XPATH^//a[text()='Analytics']";
        public static string Administation_eFormsDesigner = "XPATH^//a[text()='eForms Designer']";
        public static string Analytics_Visualizations = "XPATH^//a[text()='Visualizations']";
        public static string Analytics_JReview = "XPATH^//a[text()='JReview']";
    }


    class Study_MenuBar
    {

        public static string Study = "XPATH^//div[@class='fresh-tilled-soil']/nav/a";
        public static string DataSource = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[2]";
        public static string Import = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[3]";
        public static string Export = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[4]";
        public static string Mapper = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[5]";
        public static string Snapshot = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[6]";
        public static string Reporter = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[7]";
        public static string Task = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[8]";
        public static string eDrive = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[9]";
        public static string DefineXML = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[10]";
        public static string eFOrms = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[11]";
        public static string DMHub = "XPATH^//div[@class='fresh-tilled-soil']/nav/a[12]";
    }
}
