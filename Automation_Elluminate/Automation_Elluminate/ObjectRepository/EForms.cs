﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate.ObjectRepository
{
    class EForms
    {
        //eForms page WebElements
        public static string VerifyEformPage = "XPATH^//div[@class='panel-title']";

        public static string StudyDrpDwn_Global = "XPATH^//select[@name='study-dropdown']";
        public static string AddRecord = "XPATH^//button[@title='New Entry']";
        public static string EditRecord = "XPATH^//button[@title='Edit Entry']";
        public static string DeleteRecord = "XPATH^//button[@title='Delete Entry']";
        public static string DelRecConfirm = "XPATH^//button[text()='Yes']";
        public static string DelRecCancel = "XPATH^//button[text()='No']";
        public static string EformsTable_Global = "//*[@id='data-entry-application']/de-application-component/div[3]/div/div[2]/ng-view/dumb-records-viewer-component/div[2]/div/div/table/tbody/tr";

        public static string SelectCategory = "//span[text()='Category']";
        public static string SelectForm = "//a[text()='Form']";
        public static string InputTextData = "XPATH^//input[@type='text']";

        public static string RecordTable = "//div[@class='table-responsive']/table/tbody/tr";

        //eForms designer page WebElements
        public static string EFormDesgn_Publish = "XPATH^//button[@title='Publish All']";
        public static string AddForm = "XPATH^//span[text()='add_box']";
        public static string SaveForm = "XPATH^//button[@ng-click='$ctrl.save()']";
        public static string CancelForm = "XPATH^//button[@ng-click='$ctrl.cancel()']";
        public static string DeleteForm = "XPATH^//button[@title='Delete Form']";
        public static string DeleteFormConfirm = "XPATH^//button[@class='btn btn-primary' and text()='Yes']";

        public static string FormName = "ID^name-input";
        public static string FormLabel = "XPATH^//input[@id='label-input' and @ng-model='$ctrl.editForm.Label']";
        public static string FormDataStore = "XPATH^//i[@class='caret pull-right']";
        public static string FormCategory = "XPATH^//input[@id='label-input' and @ng-model='$ctrl.editForm.Category']";
        public static string SelectDataStore = "XPATH^//span[text()='DataStore']";
        public static string SelectStudy = "//option[text()='study']";

        public static string AddField = "XPATH^//button[@title='New Field']";
        public static string EditField = "XPATH^//button[@title='Edit Field']";
        public static string DeleteField = "XPATH^//button[@title='Delete Field']";

        public static string FieldName = "ID^name-input";
        public static string FieldLabel = "ID^label-input";
        public static string FieldCntrlDrpdwn = "XPATH^//i[@class='caret pull-right']";
        public static string FieldCntrlType = "//span[text()='ControlType']";
        public static string FieldDisplayOrd = "ID^sort-order-input";
        public static string SaveField = "XPATH^//button[@ng-click='$ctrl.save($ctrl.editField)']";

        
    }
}
