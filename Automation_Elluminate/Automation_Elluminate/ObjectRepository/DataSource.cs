﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
    
    class DataSources
    {
        public static string Datasource = "XPATH^//nav[@class='sub-nav navbar-fixed-top navbar-fixed-secondary']/a/span[contains(text(),'Data Sources')]";
        public static string DatasourceCheck = "XPATH^//table/tbody/tr[1]/td[1]/span";
        public static string Verify_DataSourcePage = "XPATH^//div[text()='Data Sources']";
        public static string DeleteDatasource = "ID^del-importsource";
        //public static string New = "XPATH^//div[@class='page']/div[@id='main']//div[@id='datasource-main']/div[@class='row'][1]/div//button[3]";
        public static string New = "XPATH^//span[text()='add_box']";
        public static string Header = "XPATH^//h4";
        public static string Typedrpdwn = "XPATH^//div[@placeholder='Select a Data Source Type']/span[@class='btn btn-default form-control ui-select-toggle']/i[@class='caret pull-right']"; 
        /* FTP,FTP Outbound, Medrio Web Service , Merge eCLinicalOS Web Services,  Rave BioStats Gateway,
         Rave ODM Adaptor,  Rave Web Services*/
        public static string Name = "id^importsource-name";
        public static string AddressUrl = "ID^importsource-address";
        public static string UserName = "ID^importsource-loginid";
        public static string Password = "ID^importsource-loginpassword";        
        public static string Description = "ID^importsource-description";
        public static string Save = "XPATH^//button[contains(text(),'Save')]";
        public static string Cancel = "XPATH^//button[contains(text(),'Cancel')]";


        public static string DSCheckbox = "XPATH^//tbody/tr[1]/td[1]/span[text()='check_box_outline_blank']";
        public static string DSDeletebutton = "XPATH^//span[text()='delete']";
        public static string DSYesbutton = "XPATH^//button[text()='Yes']";

        //FTP 
        public static string clkFTP = "XPATH^//span[text()='FTP']";
        public static string Remotefoldername = "ID^importsource-remotefoldername";
        public static string Remotefiles = "ID^importsource-remotefiles";

        //FTP OutBound
        //importsource-remotefoldername
        public static string clkFTPOutBound = "XPATH^//span[text()='FTP Outbound']";

        //Medrio Web Service 
        public static string clkMedrio = "XPATH^//span[text()='Medrio Web Service']";
        public static string apikey = "ID^importsource-apikey";

        //Merge eCLinicalOS Web Services
        public static string clkMergeeclinical = "XPATH^//span[text()='Merge eClinicalOS Web Services']";
        public static string RemoteStudy = "ID^importsource-remotestudy";

        //Rave BioStats Gateway //Rave ODM Adaptor //Rave Web Services
        public static string clkRaveBiostat = "XPATH^//span[text()='Rave Biostats Gateway']";
        public static string clkRaveODM = "XPATH^//span[text()='Rave ODM Adaptor']";
        public static string clkRaveWebService = "XPATH^//span[text()='Rave Web Services']";
        public static string Environment = "id^importsource-remoteenvironment";

        

    }
   
    

    
}
