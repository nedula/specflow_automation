﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{   

    class Mapper
    {
        public static string MapperTab = "XPATH^//a[@data-name='mapper']/span";

        public static string DefinationExists= "//*[@id='main-tab-content']/ui-layout-container[1]//table/tbody/tr";
        public static string DomainExists = "//*[@id='main-tab-content']/ui-layout-container[2]//table/tbody/tr";

        public static string New_Defination = "ID^mapper-new";
        public static string Edit_Defination = "ID^mapper-edit";
        public static string Delete_Defination = "ID^mapper-delete";
        public static string Migrate_Defination = "ID^mapper-migrate";
        public static string Export_Defination = "ID^mapping-export";
        public static string Import_Defination = "ID^mapping-import";
        public static string Execute_Defination = "XPATH^//div[@class='pull-right']/button[@id='mapper-execute']";
        public static string Executeconfirm_Defination = "ID^exec-mapping-yes";
        public static string Mapping_Defination = "XPATH^//a[text()='Mapping']";
        public static string Verify_MapperHomePage = "XPATH^//div[@id='mapper-nav']/div[text()='Mappings']";
        public static string Verify_Mapper = "XPATH^//table[@class='table table-hover table-show-selected']/tbody/tr/td/span[text()='Mapper_Defination']";
        public static string Verify_MappingStatus = "XPATH^//span[@ng-bind='mapping.Status' and text()='Completed']";


        public static string Name_Defination = "XPATH^//input[@ng-model='model.Name']";
        public static string DataStore_Defination = "XPATH^//Span[text()='Select a Data Store']";
        public static string DataStoreType = "XPATH^//span[text()='DataStore']";
        public static string Save_Defination = "ID^mapping-edit-save";
        public static string Cancel_Defination = "ID^mapping-edit-cancel";
        public static string ActiveCheckBox = "XPATH^//div[@class='modal-content']//input[@type='checkbox']";

        public static string New_Domain = "ID^domain-new";
        public static string Edit_Domain = "ID^domain-edit";
        public static string Delete_Domain = "ID^domain-delete";
        public static string Export_Domain = "ID^domain-export";
        public static string Import_Domain = "ID^domain-import";
        public static string Execute_Domain = "ID^domain-report";
        public static string Mapping_Domain = "ID^mapper-execute";
        public static string Verify_Domain = "XPATH^//table[@class='table table-hover table-show-selected']/tbody/tr/td/span[text()='Mapper_Domain']";

        public static string Active_Domain = "ID^new-domain-active";
        public static string Description = "XPATH^//textarea[@ng-model='model.Description']";
        public static string COntinue = "XPATH^//button[text()='Continue']";
        public static string Clk_ClinicalDatastore = "XPATH^//span[text()='Clinical']";

        public static string DomainA = "id^MapperTest_src:Clinical:AE";
        public static string Dashboard = "id^canvas";
        public static string SelectDomain = "XPATH^//div[@id='mapper-nav']//div[2]";
        public static string SelectJoin = "XPATH^//a[text()='Join']";
        public static string DomainB = "id^MapperTest_src:Clinical:DM";
        public static string Dashboard2 = "id^_??";
        public static string Join = "XPATH^//div[@id='key-Join1']/div";
        public static string JoinCondition2 = "XPATH^//tr/td[contains(text(),'Join Condition')]/following-sibling::td/input[2]";
        public static string PrimaryKey1 = "XPATH^//table[@id='editJoinCondition']//tr[2]/td[1]/select/option[2]";
        public static string PrimaryKey2 = "XPATH^//table[@id='editJoinCondition']//tr[2]/td[3]/select/option[2]";
        public static string AddKey = "XPATH^//table[@id='editJoinCondition']//tr[2]/td[4]/button[1]";
        public static string SaveCondition = "XPATH^//button[text()='Save']";
        public static string Savebutton = "XPATH^//div[@id='mapper-nav']//button[6]";
        public static string JoinType = "XPATH^//tr/td[contains(text(),'Join Type')]/following-sibling::td/select";

        public static string ButtonYes = "XPATH^//button[text()='Yes']";
    }
}
