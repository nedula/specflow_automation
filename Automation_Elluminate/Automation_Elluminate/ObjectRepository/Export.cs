﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
   
    class ExportFile
    {
        public static string ExportHeader = "XPATH^//a[@data-name='exporter']";
        public static string DataStore = "XPATH^//option[text()='DataStore']";
        //public static string Clinical = "XPATH^//option[text()='Clinical']";
        //public static string ODM_Mapped = "XPATH^//option[text()='ODM_Mapped']";
        //public static string ODM_stage = "XPATH^//option[text()='ODM_Stage']";
        //public static string Reporting = "XPATH^//option[text()='Reporting']";
        public static string Format = "XPATH^//div[@class='form-group']/select";
        public static string Delimited = "XPATH^//option[@value='Delimited']";
        public static string Export = "XPATH^//button[contains(text(),'Export')]";
        public static string Schedule = "XPATH^//button[contains(text(),'Schedule')]";
        public static string ExportFTPcheckbox = "XPATH^//label[@class='checkbox-inline']/input";
        public static string SelectDomain1 = "XPATH^//select[@multiple='multiple']/option[1]";
        public static string SelectDomain2 = "XPATH^//select[@multiple='multiple']/option[2]";
        public static string SelectDomain3 = "XPATH^//select[@multiple='multiple']/option[3]";
        public static string Delimited_Tilde = "XPATH^//input[@value='Tilde']";
        public static string Delimited_VerticalBar = "XPATH^//input[@value='VerticalBar']";
        public static string Domain = "XPATH^//option[text()='Domain']";
        public static string Download = "XPATH^//div/h5[text()='Recent Exports']/following-sibling::div[1]/div/a";
    }

}
