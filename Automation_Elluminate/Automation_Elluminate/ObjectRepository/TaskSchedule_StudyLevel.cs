﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation_Elluminate
{
    class TaskSchedule
    {

        public static string AddTask = "XPATH^//a[text()=' Add Task']";

        public static string TaskName = "ID^Name";

        public static string ScheduleNext = "ID^taskwizard-next";
        public static string ScheduleTime = "ID^StartDateTime";

        public static string TaskAction = "ID^task-action-types";
        public static string ScheduleClose = "ID^taskwizard-close";
        public static string Task = "XPATH^//a[@data-name='tasks']";

        public static string ImportDefination = "XPATH^//input[@name='ImportDefinitionId']";
        public static string IntegrateDiscrepancies = "ID^IntegrateDiscrepancies";
        public static string IncrementalImport = "ID^IncrementalImport";
        public static string TaskscheduleTable = "//div[@class='t-grid-content']/table/tbody/tr";

        public static string Save = "XPATH^//input[@value='Save']";
        
        public static string Delete = "XPATH^//input[@value='Delete']";
        public static string Run = "XPATH^//input[@value='Run']";

        public static string DefaultTaskName = "XPATH^//div[@id='task-wizard-pane']//tr[1]/td[2]/div";

    }
}
