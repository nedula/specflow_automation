﻿using System;
using TechTalk.SpecFlow;
using System.Data;
using TechTalk.SpecFlow.Assist;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class Elluminate_MetaDataSteps
    {
        [Given(@"I should Select Study called ""(.*)""")]
        public void GivenIShouldSelectStudyCalled(string studyName)
        {
            String Study = AddStudy.ClickStudy.Replace("Study", studyName);
            Actiontype.clickAndWait(AddStudy.ClickStudy, "5000");
            Actiontype.VerifyNotPresentAndClick(ImportFile.ImportHeader, Study);
            Actiontype.WaitUntilElementPresent(ImportFile.ImportHeader, 120);
            Console.WriteLine("Navigation to study successfull");
        }
        
        [Given(@"I Should Import data using Manual File Import")]
        public void GivenIShouldImportDataUsingManualFileImport()
        {
            Actiontype.clickAndWait(ImportFile.ImportHeader, "2000");
            Actiontype.WaitUntilElementPresent(ImportFile.New, 120);
            Actiontype.clickAndWait(ImportFile.ImportHeader, "2000");
            Actiontype.clickAndWait(ImportFile.manualFileImport, "2000");
            Actiontype.clickAndWait(ImportFile.Fileupload, "1000");
            Actiontype.Uploadfile(Startupfile.ManualFileImportSASXPORT);
            Actiontype.WaitUntilElementNotPresent(ImportFile.Downloading, 100);
            Actiontype.VerifyElementPresent(ImportFile.ReadyToIntegrate);
            Actiontype.clickAndWait(ImportFile.Integrate, "5000");
            Actiontype.clickAndWait(DataSources.DSYesbutton, "10000");
            Actiontype.WaitUntilElementNotPresent(ImportFile.ArchiveFile, 800);
            Actiontype.VerifyElementPresent(ImportFile.IntegrateComplete);
        }
        
        [Given(@"Select Browse tab")]
        public void GivenSelectBrowseTab()
        {
            Actiontype.clickAndWait(MetaData.Browse, "1000");
        }
        
        [Given(@"Select Search tab")]
        public void GivenSelectSearchTab()
        {
            Actiontype.clickAndWait(MetaData.Search, "1000");
        }
        
        [When(@"Go to MetaData home page")]
        public void WhenGoToMetaDataHomePage()
        {
            Actiontype.clickAndWait(ElluminateHome.Metadata, "3000");
            Actiontype.VerifyElementPresent(MetaData.Browse);
        }
        
        [When(@"I select Study ""(.*)"" from metadata studies list")]
        public void WhenISelectStudyFromMetadataStudiesList(string StudyName)
        {
            String SelectStudy = MetaData.SelectStudy.Replace("StudyName", StudyName);            
            Actiontype.clickAndWait(SelectStudy, "1000");
        }

        [When(@"I select Study ""(.*)"" from metadata search studies list")]
        public void WhenISelectStudyFromMetadataSearchStudiesList(string StudyName)
        {
            String SelectStudy = MetaData.Search_Study.Replace("Study", StudyName);
            Actiontype.clickAndWait(SelectStudy, "1000");
        }

        [When(@"I Have expand DataStores")]
        public void WhenIHaveExpandDataStores()
        {
            Actiontype.clickAndWait(MetaData.Browser_Expand_DataStore, "1000");
        }
        
        [When(@"I Have expand Domain name")]
        public void WhenIHaveExpandDomainName()
        {
            Actiontype.clickAndWait(MetaData.Browser_Expand_Domain, "1000");
        }


        [When(@"I select For ""(.*)"" WithIn ""(.*)"" and Condition ""(.*)"" value ""(.*)""")]
        public void WhenISelectForWithInAndConditionValue(string For, string WithIn, string Condition, string value)
        {
            if (For.ToUpper().Equals("DOMAIN") && WithIn.ToUpper().Equals("NAMES") && Condition.ToUpper().Equals("START WITH"))
            {
                Actiontype.EnterValue(MetaData.Search_SearchText, value);
                Actiontype.clickAndWait(MetaData.Search_Submit, "2000");
            }
            else if (For.ToUpper().Equals("VARIABLES") && WithIn.ToUpper().Equals("LABELS") && Condition.ToUpper().Equals("ENDS WITH"))
            {
                Actiontype.clickAndWait(MetaData.Search_ForVariables, "2000");
                Actiontype.clickAndWait(MetaData.Search_WithinLabels, "2000");
                Actiontype.clickAndWait(MetaData.Search_ConditionEndsWith, "2000");
                Actiontype.EnterValue(MetaData.Search_SearchText, value);
                Actiontype.clickAndWait(MetaData.Search_Submit, "2000");
            }
        }


              
        [Then(@"I should see Study called ""(.*)"" in metadata studies list")]
        public void ThenIShouldSeeStudyCalledInMetadataStudiesList(string StudyName)
        {
            String SelectStudy = MetaData.SelectStudy.Replace("StudyName", StudyName);
            Actiontype.VerifyElementPresent(SelectStudy);
        }
        [Then(@"I should see Study called ""(.*)"" in metadata search studies list")]
        public void ThenIShouldSeeStudyCalledInMetadataSearchStudiesList(string StudyName)
        {
             String SelectStudy = MetaData.Search_Study.Replace("StudyName", StudyName);
            Actiontype.VerifyElementPresent(SelectStudy);
        }



        [Then(@"I should see Imported DataStore in the right side page with columns")]
        public void ThenIShouldSeeImportedDataStoreInTheRightSidePageWithColumns(Table table)
        {
           
            foreach (var header in table.Header)
            {
                String element ="XPATH^//th[text()='"+header+"']";
                Actiontype.VerifyElementPresent(element);
            }
        }

        [Then(@"I should see all Domain lists with columns")]
        public void ThenIShouldSeeAllDomainListsWithColumns(Table table)
        {
            foreach (var header in table.Header)
            {
                String element = "XPATH^//th[text()='" + header + "']";
                Actiontype.VerifyElementPresent(element);
            }
        }


        [Then(@"I should see all Variables lists")]
        public void ThenIShouldSeeAllVariablesLists(Table table)
        {
            foreach (var header in table.Header)
            {
                String element = "XPATH^//th[text()='" + header + "']";
                Actiontype.VerifyElementPresent(element);
            }
        }


        [Then(@"the result should be display with columns in Results section")]
        public void ThenTheResultShouldBeDisplayWithColumnsInResultsSection(Table table)
        {
            foreach (var header in table.Header)
            {
                String element = "XPATH^//a[text()='" + header + "']";
                Actiontype.VerifyElementPresent(element);
            }
        }

        [Then(@"Veify all results Startwith DomainName ""(.*)""")]
        public void ThenVeifyAllResultsStartwithDomainName(string DomainName)
        {
            
        }
        [Then(@"Veify all results Endswith VariableName ""(.*)""")]
        public void ThenVeifyAllResultsEndswithVariableName(string VariableName)
        {
            
        }



        [Given(@"Select Compare tab")]
        public void GivenSelectCompareTab()
        {
            Actiontype.clickAndWait(MetaData.Compare, "2000");
        }

        [Then(@"I should see Study called ""(.*)"" in studies list")]
        public void ThenIShouldSeeStudyCalledInStudiesList(string StudyName)
        {
            String TempElement = MetaData.Compare_Study.Replace("Study", StudyName);
            Actiontype.VerifyElementPresent(TempElement);
        }

        [When(@"I select Study ""(.*)"" from studies list")]
        public void WhenISelectStudyFromStudiesList(string StudyName)
        {
            String TempElement = MetaData.Compare_Study.Replace("Study", StudyName);
            Actiontype.clickAndWait(TempElement,"3000");
        }

        [When(@"I select Domain ""(.*)"" from Domain list")]
        public void WhenISelectDomainFromDomainList(string DomainName)
        {
            String TempElement = MetaData.Compare_Domain.Replace("Domain", DomainName);
            Actiontype.clickAndWait(TempElement, "3000");
        }

        [Then(@"Data should load for selected study and domin")]
        public void ThenDataShouldLoadForSelectedStudyAndDomin()
        {
            
        }

        [Given(@"I Select DataStore for compare")]
        public void GivenISelectDataStoreForCompare()
        {
            Actiontype.clickAndWait(MetaData.Compare_Staging, "1000");
        }


    }
}
