﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    class CommonScenariosSteps
    {
        [Given(@"We are at the Sign In Page")]
        public void GivenWeAreAtTheSignInPage()
        {
            Actiontype.OPenUrl();
        }

        [Given(@"I login ""(.*)"", ""(.*)"" and ""(.*)""")]
        public void GivenILoginAnd(string UserName, string Password, string ProfileName)
        {
            Actiontype.loginmethod(UserName, Password, ProfileName);
        }
        [Given(@"I login with user called ""(.*)""")]
        public void GivenILoginWithUserCalled(string UserName)
        {
            if (UserName.ToUpper().Equals(LoginUser1.Username.ToUpper()))
            {
                Actiontype.EnterValue(Login.Username, UserName);
                Actiontype.EnterValue(Login.Password, LoginUser1.Password);
                Actiontype.clickAndWait(Login.Sumbit, "3000");
                Actiontype.OPenUrl();
                Actiontype.VerifyAttributeValue(Login.LoginPerson, "Title", LoginUser1.ProfileName);
            }
        }
        [When(@"I login with user called ""(.*)""")]
        public void WhenILoginWithUserCalled(string UserName)
        {
            if (UserName.ToUpper().Equals(LoginUser2.Username.ToUpper()))
            {
                Actiontype.EnterValue(Login.Username, UserName);
                Actiontype.EnterValue(Login.Password, LoginUser2.Password);
                Actiontype.clickAndWait(Login.Sumbit, "3000");
                Actiontype.OPenUrl();
                Actiontype.VerifyAttributeValue(Login.LoginPerson, "Title", LoginUser2.ProfileName);
            }
        }

        [Given(@"Go to Home Page")]
        public void GivenGoToHomePage()
        {
            Actiontype.clickAndWait(ElluminateHome.StudyHome, "3000");
            Actiontype.VerifyElementPresent(ElluminateHome.RecentStudies);
        }

    }
}
