﻿using Automation_Elluminate.ObjectRepository;
using System;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class Elluminate_EDriveSteps
    {
        [Given(@"select eDrive Tab")]
        public void GivenSelectEDriveTab()
        {
            Actiontype.clickAndWait(ElluminateHome.eDrive, "3000");
        }

        [Then(@"I should see eDrive home page")]
        public void ThenIShouldSeeEDriveHomePage()
        {
            Actiontype.VerifyElementPresent(EDrive.Verify_eDriveHomePage);
            Console.WriteLine("Navigation to eDrive home page successful");
        }

        [When(@"I Create new folder called ""(.*)"" for ""(.*)"" type")]
        public void WhenICreateNewFolderCalledForType(string Foldername, string Foldertype)
        {
            Actiontype.clickAndWait(EDrive.CreateFolder, "3000");
            Actiontype.EnterValue(EDrive.FolderName, Foldername);
            if (Foldertype.Equals("VersionControl"))
            {
                Actiontype.click(EDrive.UseVersionControl);
                Actiontype.clickAndWait(EDrive.CreateFolderSubmit, "4000");
                Console.WriteLine("Folder creation successful: " + Foldername);
            }
            if (Foldertype.Equals("NonVersionControl"))
            {
                Actiontype.clickAndWait(EDrive.CreateFolderSubmit, "4000");
                Console.WriteLine("Folder creation successful: " + Foldername);
            }

        }

        [Then(@"Verify ""(.*)"" under folders section")]
        public void ThenVerifyUnderFoldersSection(string Foldername)
        {
            Actiontype.VerifyElementPresent("XPATH^//span[@class='t-in' and text()='" + Foldername + "']");
            Console.WriteLine("Folder is present");
        }

        [Then(@"Upload a file for ""(.*)"" folder for ""(.*)""")]
        public void ThenUploadAFileForFolderFor(string Foldername, string Foldertype)
        {
            String Folder = EDrive.Click_Folder.Replace("Foldername", Foldername);
            Actiontype.clickAndWait(Folder, "5000");
            Actiontype.clickAndWait(EDrive.UploadToFolder, "4000");
            Actiontype.clickAndWait(EDrive.ChooseFile, "3000");
            Actiontype.Uploadfile(EDrive.File);
            if (Foldertype.Equals("VersionControl"))
            {
                Actiontype.EnterValue(EDrive.Comments, "xlsx file");
                Actiontype.clickAndWait(EDrive.UploadConfirm, "3000");
                Actiontype.WaitUntilElementPresent(EDrive.Verify_FileUploaded, 6000);
                Actiontype.clickAndWait(EDrive.UploadClose, "4000");
                Console.WriteLine("File uploaded successfully");
                Actiontype.VerifyElementPresent(EDrive.Verify_Version);
                Console.WriteLine("File uploaded with version # as 1");
            }
            if (Foldertype.Equals("NonVersionControl"))
            {
                Actiontype.WaitUntilElementPresent(EDrive.Verify_FileUploaded, 6000);
                Actiontype.clickAndWait(EDrive.Close_btn, "4000");
                Console.WriteLine("File uploaded successfully");
            }
        }

        [When(@"Click on ""(.*)"" folder")]
        public void WhenClickOnFolder(string Foldername)
        {
            String Folder = EDrive.Click_Folder.Replace("Foldername", Foldername);
            Actiontype.clickAndWait(Folder, "5000");
        }
        [Then(@"Expand ""(.*)"" folder")]
        public void ThenExpandFolder(string Foldername)
        {
            String Expand = EDrive.Clk_tIcon.Replace("Foldername", Foldername);
            Actiontype.clickAndWait(Expand, "2000");
        }

        [Then(@"Delete ""(.*)"" folder")]
        public void ThenDeleteFolder(string Foldername)
        {
            String Folder = EDrive.Click_Folder.Replace("Foldername", Foldername);
            Actiontype.clickAndWait(Folder, "5000");
            Actiontype.clickAndWait(EDrive.DeleteFolder, "2000");
            Actiontype.clickAndWait(EDrive.DeleteFolderConfirm, "2000");
            Actiontype.VerifyElementNotPresent("XPATH^//span[@class='t-in' and text()='" + Foldername + "']");
            Console.WriteLine("Folder is deleted");
        }
        [Then(@"I Rename Folder ""(.*)"" to ""(.*)""")]
        public void ThenIRenameFolderTo(string Folder1, string Folder2)
        {
            Actiontype.clickAndWait(EDrive.RenameFolder, "2000");
            Actiontype.EnterValue(EDrive.FolderName, Folder2);
            Actiontype.clickAndWait(EDrive.RenameFolderConfirm, "4000");
            Console.WriteLine("Folder renamed as: " + Folder2);
        }

        [Then(@"Verify deleted folder ""(.*)""")]
        public void ThenVerifyDeletedFolder(string Foldername)
        {
            Actiontype.VerifyElementNotPresent("XPATH^//span[@class='t-in' and text()='" + Foldername + "']");
            Console.WriteLine("Folder deleted successfully");
        }

        [Then(@"I Copy the ""(.*)"" file to ""(.*)"" folder")]
        public void ThenICopyTheFileToFolder(string Filename, string Foldername)
        {
            Actiontype.click(EDrive.SelectCheckbox);
            Actiontype.clickAndWait(EDrive.CopyFile, "2000");
            String Folder1 = EDrive.Click_FolderTree.Replace("Foldername", Foldername);
            Actiontype.clickAndWait(Folder1, "5000");
            Actiontype.clickAndWait(EDrive.CopyConfirm, "2000");
            Actiontype.clickAndWait(EDrive.Copy_OK_btn, "3000");
            Actiontype.clickAndWait(EDrive.Close_btn, "2000");
            String Folder2 = EDrive.Click_Folder.Replace("Foldername", Foldername);
            Actiontype.clickAndWait(Folder2, "5000");
            Actiontype.VerifyElementPresent("XPATH^//td[text()='" + Filename + "']");
            Console.WriteLine("File Copied successfully" + Filename);
        }


        [Then(@"Then I Download the file")]
        public void ThenThenIDownloadTheFile()
        {
            Actiontype.click(EDrive.SelectCheckbox);
            Actiontype.clickAndWait(EDrive.DownloadFile, "8000");
            Console.WriteLine("File downloaded successfully");
        }
        [Then(@"Move the ""(.*)"" file to ""(.*)"" folder")]
        public void ThenMoveTheFileToFolder(string Filename, string Foldername)
        {
            Actiontype.clickAndWait(EDrive.MoveFile, "2000");
            String Folder1 = EDrive.Click_FolderTree.Replace("Foldername", Foldername);
            Actiontype.clickAndWait(Folder1, "5000");
            Actiontype.clickAndWait(EDrive.MoveConfirm, "2000");
            Actiontype.clickAndWait(EDrive.Move_OK_btn, "3000");
            Actiontype.clickAndWait(EDrive.Close_btn, "2000");
            Actiontype.VerifyElementNotPresent("XPATH^//td[text()='" + Filename + "']");
            String Folder2 = EDrive.Click_Folder.Replace("Foldername", Foldername);
            Actiontype.clickAndWait(Folder2, "5000");
            Actiontype.VerifyElementPresent("XPATH^//td[text()='" + Filename + "']");
            Console.WriteLine("File moved successfully");
        }

        [Then(@"I Rename the file ""(.*)"" to ""(.*)""")]
        public void ThenIRenameTheFileTo(string Filename1, string Filename2)
        {
            Actiontype.click(EDrive.SelectCheckbox);
            Actiontype.clickAndWait(EDrive.RenameFile, "2000");
            Actiontype.EnterValue(EDrive.FileName, Filename2);
            Actiontype.clickAndWait(EDrive.RenameFileConfirm, "3000");
            Actiontype.VerifyElementPresent("XPATH^//td[text()='" + Filename2 + "']");
            Console.WriteLine("File renamed successfully");
        }

        [Then(@"I Delete the file ""(.*)""")]
        public void ThenIDeleteTheFile(string Filename)
        {
            Actiontype.click(EDrive.SelectCheckbox);
            Actiontype.clickAndWait(EDrive.DeleteFile, "2000");
            Actiontype.clickAndWait(EDrive.DeleteFileConfirm, "4000");
            Actiontype.VerifyElementNotPresent("XPATH^//td[text()='" + Filename + "']");
            Console.WriteLine("File deleted successfully");
        }

        [Then(@"Perform check out")]
        public void ThenPerformCheckOut()
        {
            Actiontype.click(EDrive.SelectCheckbox);
            Actiontype.clickAndWait(EDrive.CheckOutFile, "2000");
            Actiontype.EnterValue(EDrive.Comments, "check out file");
            Actiontype.clickAndWait(EDrive.CheckOutConfirm, "3000");
            Actiontype.VerifyElementPresent(EDrive.Verify_CheckedOutTo);
            Console.WriteLine("File is successfully checked out");
        }

        [Then(@"Undo check out")]
        public void ThenUndoCheckOut()
        {
            Actiontype.click(EDrive.SelectCheckbox);
            Actiontype.clickAndWait(EDrive.UndoCheckOut, "2000");
            Actiontype.EnterValue(EDrive.Comments, "Undo out file");
            Actiontype.clickAndWait(EDrive.UndoCheckOutConfirm, "6000");
            Actiontype.VerifyElementNotPresent(EDrive.Verify_CheckedOutTo);
            Console.WriteLine("Undo check out performed successfully");
            Actiontype.clickAndWait(EDrive.DeleteFolder, "2000");
            Actiontype.clickAndWait(EDrive.DeleteFolderConfirm, "2000");
        }

        [Then(@"Perform check in")]
        public void ThenPerformCheckIn()
        {
            Actiontype.click(EDrive.SelectCheckbox);
            Actiontype.clickAndWait(EDrive.CheckOutFile, "2000");
            Actiontype.EnterValue(EDrive.Comments, "check out file");
            Actiontype.clickAndWait(EDrive.CheckOutConfirm, "3000");
            Actiontype.VerifyElementPresent(EDrive.Verify_CheckedOutTo);
            Console.WriteLine("File is successfully checked out");

            Actiontype.click(EDrive.SelectCheckbox);
            Actiontype.clickAndWait(EDrive.CheckInFile, "2000");
            Actiontype.clickAndWait(EDrive.ChooseFile, "3000");
            Actiontype.Uploadfile(EDrive.File);
            Actiontype.EnterValue(EDrive.Comments, "xlsx file");
            Actiontype.clickAndWait(EDrive.CheckInConfirm, "3000");
            Actiontype.WaitUntilElementPresent(EDrive.Verify_FileUploaded, 6000);
            Actiontype.clickAndWait(EDrive.UploadClose, "4000");
            Actiontype.VerifyElementPresent(EDrive.Verify_VersionUpdated);
            Console.WriteLine("File is successfully checked in");
            Actiontype.clickAndWait(EDrive.DeleteFolder, "2000");
            Actiontype.clickAndWait(EDrive.DeleteFolderConfirm, "2000");
        }

    }
}
