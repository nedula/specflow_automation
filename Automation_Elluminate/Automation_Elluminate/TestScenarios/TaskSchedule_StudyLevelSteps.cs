﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class Elluminate_TaskSchedule_StudyLevelSteps
    {
        [Given(@"Select Task tab")]
        public void GivenSelectTaskTab()
        {
            Actiontype.clickAndWait(Study_MenuBar.Task, "3000");
        }
        
        [Given(@"click on Add Task link")]
        public void GivenClickOnAddTaskLink()
        {
            Actiontype.clickAndWait(TaskSchedule.AddTask, "3000");
        }
        
        [Then(@"Created Task With name ""(.*)"" , Type ""(.*)"" , Action ""(.*)""")]
        public void ThenCreatedTaskWithNameTypeAction(string Name, string type, string Action)
        {
            CreateScheduleTask(Name, type, Action);
        }

        [When(@"I Edit Task  Name ""(.*)"" to ""(.*)"" , Type ""(.*)"" , Action ""(.*)""")]
        public void WhenIEditTaskNameToTypeAction(string NewTask, string EditTask, string type, string Action)
        {

            EditScheduleTask(NewTask, EditTask, type, Action);
        }


        [Then(@"Verify ""(.*)"" Task is Executed successfully")]
        public void ThenVerifyTaskIsExecutedSuccessfully(string p0)
        {
            
        }


        [When(@"I Run ""(.*)"" Task")]
        public void WhenIRunTask(string TaskName)
        {
            
        }
       

        [When(@"I Delete ""(.*)"" Task")]
        public void WhenIDeleteTask(string TaskName)
        {
            DeleteScheduleTask(TaskName);
        }
                

        public static void EditScheduleTask(String NewTask, String EditTask, String TaskType, String TaskAction)
        {
            IList<IWebElement> displayedOptions = ElementInfo.driver.FindElements(By.XPath(TaskSchedule.TaskscheduleTable));
            int count = displayedOptions.Count;

            for(int i=1; i<count; i++)
            {
                String temp = ElementInfo.driver.FindElement(By.XPath(TaskSchedule.TaskscheduleTable +"[" + i + "]/td[2]")).Text;

                if (NewTask.Contains(temp) || temp.Contains(NewTask))
                {

                    Actiontype.clickAndWait("XPATH^" + TaskSchedule.TaskscheduleTable + "[" + i + "]/td[9]","3000");

                    CreateScheduleTask(EditTask, TaskType, TaskAction);

                    i = i + count;
                }
                
            }

        }

        public static void DeleteScheduleTask(String TaskName)
        {
            IList<IWebElement> displayedOptions = ElementInfo.driver.FindElements(By.XPath(TaskSchedule.TaskscheduleTable));
            int count = displayedOptions.Count;

            for (int i = 1; i < count; i++)
            {
                String temp = ElementInfo.driver.FindElement(By.XPath(TaskSchedule.TaskscheduleTable + "[" + i + "]/td[2]")).Text;

                if (TaskName.Contains(temp) || temp.Contains(TaskName))
                {

                    Actiontype.clickAndWait("XPATH^" + TaskSchedule.TaskscheduleTable + "[" + i + "]/td[10]", "3000");

                    Actiontype.clickAndWait(TaskSchedule.Delete, "2000");
                    i = i + count;
                }

            }

            IList<IWebElement> AfterDeleteOptions = ElementInfo.driver.FindElements(By.XPath(TaskSchedule.TaskscheduleTable));
            int Aftercount = AfterDeleteOptions.Count;

            if(count==Aftercount+1)
            {
                Console.WriteLine("DeleteScheduleTask executed sucessfully");
            }
            else
            {
                Console.WriteLine("DeleteScheduleTask failed due to delete before and after count not matching \r\n" + "Before COunt :" + count  + "\r\n"+ " afterCount"+Aftercount );
            }

        }
        public static void CreateScheduleTask(String TaskName, String TaskType, String TaskAction)
        {

            String Name = TaskName + Actiontype.RandomString(5);

            Actiontype.EnterValue(TaskSchedule.TaskName, Name);

            Actiontype.clickAndWait(TaskSchedule.ScheduleNext, "3000");

            if (TaskType == "OneTime")
            {
                Actiontype.clickAndWait(TaskSchedule.ScheduleNext, "3000");
                String SecheduleTime = Actiontype.GetAttributeValue(ImportFile.ScheduleTime, "value");
                DateTime NewSecheduleTime = DateTime.ParseExact(SecheduleTime, "yyyy-MMM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                NewSecheduleTime = NewSecheduleTime.AddHours(-1);
                NewSecheduleTime = NewSecheduleTime.AddMinutes(2);
                String newdate = NewSecheduleTime.ToString("yyyy-MMM-dd HH:mm:ss");
                Actiontype.ClearText(ImportFile.ScheduleTime);
                Actiontype.EnterValue(ImportFile.ScheduleTime, newdate);
                Actiontype.clickAndWait(ImportFile.ScheduleNext, "1000");
                Actiontype.SelectDropdownValue(TaskSchedule.TaskAction, "VALUE", TaskAction);
                Actiontype.clickAndWait(TaskSchedule.ImportDefination, "1000");
                Actiontype.clickAndWait(TaskSchedule.Save, "3000");
                Actiontype.clickAndWait(ImportFile.ScheduleNext, "1000");
                Actiontype.clickAndWait(ImportFile.ScheduleClose, "2000");
            }
            else if (TaskType == "Hourly")
            {
            }
            else if (TaskType == "Daily")
            {
            }
            else if (TaskType == "Weekly")
            {
            }
            else if (TaskType == "Monthly")
            {
            }



            if (TaskAction == "Import")
            {
            }
            else if (TaskAction == "Export")
            {
            }

        }
    }
}
