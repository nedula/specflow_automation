﻿using Automation_Elluminate.ObjectRepository;
using System;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class DefineXMLSteps
    {
        [Given(@"Go to DefineXML tab")]
        public void GivenGoToDefineXMLTab()
        {
            Actiontype.clickAndWait(DefineXML.DefineXMLHeader, "3000");
            Console.WriteLine("Navigation to Define XML tab successful");           
        }
        
        [Then(@"Upload a Spec file")]
        public void ThenUploadASpecFile()
        {
            Functions.GetRows(DefineXML.TableGrid);
            Actiontype.clickAndWait(DefineXML.Select_Btn,"2000");
            Actiontype.Uploadfile(Startupfile.DefineXMLData);
        }

        [Then(@"Generate file without Validating")]
        public void ThenGenerateFileWithoutValidating()
        {
            Actiontype.clickAndWait(DefineXML.Generate_Btn, "3000");
            Actiontype.WaitUntilElementPresent(DefineXML.Verify_Status, 5000);
            Console.WriteLine("Verified Spec file status as 'completed'");
            
        }

        [Then(@"Generate file with Validating")]
        public void ThenGenerateFileWithValidating()
        {
            Actiontype.click(DefineXML.Validate_Checkbox);
            Actiontype.clickAndWait(DefineXML.Generate_Btn, "3000");
            Actiontype.WaitUntilElementPresent(DefineXML.Verify_Status, 5000);
            Console.WriteLine("Verified Spec file status as 'completed'");
        }

        [Then(@"Validate and Generate for Validation is ""(.*)""")]
        public void ThenValidateAndGenerateForValidationIs(Boolean Value)
        {
            Functions.ValidateDefineXMLResult(DefineXML.TableGrid,Value);
        }

        [Then(@"Define xml should be generated with Validation results is ""(.*)""")]
        public void ThenDefineXmlShouldBeGeneratedWithValidationResultsIs(Boolean Value)
        {
            Functions.ValidateDefineXMLResult(DefineXML.TableGrid, Value);
        }


        [Given(@"Go to Data Quality tab")]
        public void GivenGoToDataQualityTab()
        {
            Actiontype.clickAndWait(DataQuality.DataQualityHeader, "3000");
            Actiontype.clickAndWait(DataQuality.Validations, "2000");
            Console.WriteLine("Navigation to Data Quality - Validations page successful");
        }

        [Then(@"Verify DataQuality Validation Result")]
        public void ThenVerifyDataQualityValidationResult()
        {
            Functions.GetRows(DataQuality.ValidationRows);
        }

        [Then(@"Verify Validation result link in DefineXML")]
        public void ThenVerifyValidationResultLinkInDefineXML()
        {
            Actiontype.clickAndWait(DefineXML.Click_ValidationLink, "3000");
            Console.WriteLine("Validation Result xlsx file downloaded successfully");
        }

        [Then(@"Download XML file from Data Quality page")]
        public void ThenDownloadXMLFileFromDataQualityPage()
        {
            Actiontype.clickAndWait(DataQuality.DownloadXMLFile, "3000");
            Console.WriteLine("XML file downloaded successfully");
        }

    }
}
