﻿using System;
using System.Collections.Generic;
using System.Threading;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class Administration
    {


        [Given(@"Go to Administration, User Management")]
        public void GivenGoToAdministrationUserManagement()
        {
            Actiontype.click(ElluminateHome.Administration);
            Actiontype.clickAndWait(Users.UserManagement, "2000");
            Actiontype.VerifyElementPresent(Users.AddUser);
        }

        [When(@"I create a new User with below deatils")]
        public void WhenICreateANewUserWithBelowDeatils(Table table)
        {
            Actiontype.clickAndWait(Users.AddUser, "2000");
            Actiontype.EnterValue(Users.UserName, table.Rows[0][0].ToString());
            Actiontype.EnterValue(Users.FirstName, table.Rows[0][1].ToString());
            Actiontype.EnterValue(Users.Company, table.Rows[0][2].ToString());
            Actiontype.EnterValue(Users.LastName, table.Rows[0][3].ToString());
            Actiontype.EnterValue(Users.Email, table.Rows[0][4].ToString());
            Actiontype.click(Users.AdministerCheckbox);
            Actiontype.clickAndWait(Users.UserSavebtn, "2000");
            Console.WriteLine("User added successfully");
            Thread.Sleep(5000);
        }

       
        [Then(@"I should see user  called ""(.*)"" in user list")]
        public void ThenIShouldSeeUserCalledInUserList(string user)
        {
            Functions.UserActions(Users.UserTable, "VERIFYUSER", user, null);
        }



        [When(@"I Edit user called ""(.*)"" with below details")]
        public void WhenIEditUserCalledWithBelowDetails(string userName, Table table)
        {
            Functions.UserActions(Users.UserTable, "EDIT", userName, table);
        }


        [Then(@"I should see user  called ""(.*)"",""(.*)"" in user list")]
        public void ThenIShouldSeeUserCalledInUserList(string UserName, string EMail)
        {
            String VerifyUser = Users.VerifyUser.Replace("EMAIL", EMail);
            Actiontype.VerifyElementPresent(VerifyUser);
            ElementInfo.driver.Navigate().Refresh();
        }


        [When(@"I Delete user called ""(.*)""")]
        public void WhenIDeleteUserCalled(string userName)
        {
            Functions.UserActions(Users.UserTable, "DELETE", userName, null);
        }

        [Then(@"I should not see user called ""(.*)""")]
        public void ThenIShouldNotSeeUserCalled(string userName)
        {
            Functions.UserActions(Users.UserTable, "VERIFYUSERNOTEXIST", userName, null); 
        }

        [Given(@"I should able to disable the user called ""(.*)""")]
        public void GivenIShouldAbleToDisableTheUserCalled(string userName)
        {
            Functions.UserActions(Users.UserTable, "DISABLE", userName, null);
        }

        [When(@"I Logout from current user")]
        public void WhenILogoutFromCurrentUser()
        {
            Actiontype.logoutmethod();
        }

        [When(@"I login with disable user called ""(.*)""")]
        public void WhenILoginWithDisableUserCalled(string UserName)
        {
            Actiontype.EnterValue(Login.Username, UserName);
            Actiontype.EnterValue(Login.Password, LoginUser2.Password);
            Actiontype.clickAndWait(Login.Sumbit, "3000");
        }


        [Then(@"I should see message ""(.*)""")]
        public void ThenIShouldSeeMessage(string ErrorMessage)
        {
            Actiontype.VerifyElementPresent(Alerts.Present);

            String actualMessage = Actiontype.GetText(Alerts.AlertMessage);

            if (ErrorMessage.ToUpper().Equals(actualMessage.ToUpper()))
            {
                Console.WriteLine("Alert message for expected and actual message are mismatch \nExpected = " + ErrorMessage + "\nActual Message =" + actualMessage);
            }
            else
            {
                Variables.isTestCaseFailed = true;
                Report.ErrorMessage = "Alert message for expected and actual message are mismatch \nExpected = " + ErrorMessage + "\nActual Message =" + actualMessage;
                Console.WriteLine("Alert message for expected and actual message are mismatch \nExpected = "+ ErrorMessage+ "\nActual Message =" +actualMessage );
            }
        }

        [Given(@"I login with invalid Credentials using user called ""(.*)""")]
        public void GivenILoginWithInvalidCredentialsUsingUserCalled(string UserName)
        {
            Actiontype.EnterValue(Login.Username, UserName);
            Actiontype.EnterValue(Login.Password, "Password");
            Actiontype.clickAndWait(Login.Sumbit, "3000");
        }


        [Given(@"I should Reset disable user ""(.*)"" to Active")]
        public void GivenIShouldResetDisableUserToActive(string userName)
        {
            Functions.UserActions(Users.UserTable, "ENABLE", userName, null);
        }

        [Then(@"I should see elluminate Home page")]
        public void ThenIShouldSeeElluminateHomePage()
        {
            Actiontype.VerifyElementPresent(ElluminateHome.RecentStudies);
        }

        [Given(@"User account is locked  after ""(.*)"" unsuccessful attempt to login user called ""(.*)""")]
        public void GivenUserAccountIsLockedAfterUnsuccessfulAttemptToLoginUserCalled(int Count, string UserName)
        {
            for (int i = 0; i < Count; i++)
            {
                Actiontype.EnterValue(Login.Username, UserName);
                Actiontype.EnterValue(Login.Password, "Password");
                Actiontype.clickAndWait(Login.Sumbit, "3000");
            }
        }

       

        [Then(@"I should see Account is locked for user called ""(.*)""")]
        public void ThenIShouldSeeAccountIsLockedForUserCalled(string UserName)
        {
            Functions.UserActions(Users.UserTable, "LOCKED", UserName, null);
        }

        [Then(@"I should Unlock user called ""(.*)"" to Active User")]
        public void ThenIShouldUnlockUserCalledToActiveUser(string UserName)
        {
            Functions.UserActions(Users.UserTable, "UNLOCK", UserName, null);
        }
       

        [When(@"I should go to User My Profile page")]
        public void WhenIShouldGoToUserMyProfilePage()
        {
            Actiontype.clickAndWait(LogOut.UserMenu, "3000");
            Actiontype.clickAndWait(MyProfile.Profile, "3000");
        }

        [Then(@"I should see user information ""(.*)"",""(.*)"",""(.*)"" and ""(.*)""")]
        public void ThenIShouldSeeUserInformationAnd(string myInfo, string myPref, string MyPrivileges, string MyStudies)
        {
            
        }
        [Then(@"I should see user information ""(.*)""")]
        public void ThenIShouldSeeUserInformation(string ProfileInfo)
        {
            List<string> Information = new List<string>(ProfileInfo.Split(','));

            for(int i=0; i< Information.Count;i++)
            {

                Actiontype.VerifyElementPresent("//div[text()='"+Information[i]+"']");
            }

        }


        [When(@"I Edit Phone ""(.*)"" and Fax ""(.*)""")]
        public void WhenIEditPhoneAndFax(int Phone, int Fax)
        {
            Actiontype.EnterValue(MyProfile.PhoneNumber, Phone.ToString());
            Actiontype.EnterValue(MyProfile.Fax, Fax.ToString());
           
        }

        [When(@"I should save my Profile information")]
        public void WhenIShouldSaveMyProfileInformation()
        {
            Actiontype.clickAndWait(MyProfile.save, "3000");
        }

        [Then(@"Go to User profile and Verify  Phone ""(.*)"" and Fax ""(.*)"" is saved")]
        public void ThenGoToUserProfileAndVerifyPhoneAndFaxIsSaved(int Phone, int Fax)
        {
            Actiontype.clickAndWait(LogOut.UserMenu, "3000");
            Actiontype.clickAndWait(MyProfile.Profile, "3000");
            Actiontype.GetText(MyProfile.PhoneNumber).Equals(Phone);
            Actiontype.GetText(MyProfile.Fax).Equals(Fax);

        }



    }
}
