﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechTalk.SpecFlow;
using OpenQA.Selenium;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    class MapperSteps
    {


        [Given(@"I choose ""(.*)"" Study from studies list\.")]
        public void GivenIChooseStudyFromStudiesList_(string StudyName)
        {
            Actiontype.clickAndWait(AddStudy.tileview, "3000");
            Actiontype.EnterValue(AddStudy.searchfilter, StudyName);
            Actiontype.clickAndWait(AddStudy.Searchstudy, "2000");
            Actiontype.VerifyElementPresent("XPATH^//div[@id='#snapgridtileview']/div[1]//h1/a[contains(text(),'" + StudyName + "')]");
            String Study = AddStudy.ClickStudy.Replace("Study", StudyName);
            Actiontype.clickAndWait(Study, "5000");
        }

        [Given(@"select mapper Tab")]
        public void GivenSelectMapperTab()
        {
            Actiontype.clickAndWait(Mapper.MapperTab, "5000");
        }

        [Then(@"I should see Mapper home page")]
        public void ThenIShouldSeeMapperHomePage()
        {
            Actiontype.VerifyElementPresent(Mapper.Verify_MapperHomePage);
            Console.WriteLine("Navigation to Mapper home page successful");
        }


        [Then(@"I Should Delete Existing mapping Defination and Domain")]
        public void ThenIShouldDeleteExistingMappingDefinationAndDomain()
        {
            try
            {
                try
                {
                    ElementInfo.webElement = ElementInfo.driver.FindElement(By.XPath(Mapper.DomainExists));

                    Actiontype.clickAndWait(Mapper.Delete_Domain, "2000");
                    Actiontype.clickAndWait(Mapper.ButtonYes, "2000");
                }

                catch(Exception)
                {

                }


                ElementInfo.webElement = ElementInfo.driver.FindElement(By.XPath(Mapper.DefinationExists));

                Actiontype.clickAndWait(Mapper.Delete_Defination, "2000");
                Actiontype.clickAndWait(Mapper.ButtonYes, "2000");
            }
            catch(Exception)
            {

            }
        }


        [When(@"Create mapping Defination name called ""(.*)"" with DataStore ""(.*)""")]
        public void WhenCreateMappingDefinationNameCalledwithDataStore(string MapperDefination, string DataStore)
        {
            Actiontype.clickAndWait(Mapper.New_Defination, "3000");
            Actiontype.clickAndWait(Mapper.Mapping_Defination, "3000");
            Actiontype.EnterValue(Mapper.Name_Defination, MapperDefination);
            Actiontype.clickAndWait(Mapper.DataStore_Defination, "3000");
            String DSType = Mapper.DataStoreType.Replace("DataStore", DataStore);
            Actiontype.clickAndWait(DSType, "3000");
            Actiontype.clickAndWait(Mapper.Save_Defination, "3000");
        }

        [Then(@"I should see ""(.*)"" in mappings section")]
        public void ThenIShouldSeeInMappingsSection(string MapperDefination)
        {

        }

        [When(@"Creating mapping domain name called ""(.*)""")]
        public void WhenCreatingMappingDomainNameCalled(string MapperDomain)
        {
            Actiontype.clickAndWait(Mapper.New_Domain, "3000");
            Actiontype.EnterValue(Mapper.Name_Defination, MapperDomain);
            Actiontype.clickAndWait(Mapper.Active_Domain, "3000");
            Actiontype.EnterValue(Mapper.Description, MapperDomain);
            Actiontype.clickAndWait(Mapper.COntinue, "3000");
            Actiontype.clickAndWait(Mapper.Clk_ClinicalDatastore, "3000");
        }

        [Then(@"I should see ""(.*)"" in Domains section")]
        public void ThenIShouldSeeInDomainsSection(string MapperDomain)
        {

        }


        [Given(@"Map ""(.*)"" and ""(.*)"" using Left Join Condition")]
        public void GivenMapAndUsingLeftJoinCondition(string p0, string p1)
        {
            Actiontype.DragAndDrop(Mapper.DomainA, Mapper.Dashboard);
            Thread.Sleep(5000);
            Actiontype.clickAndWait(Mapper.SelectDomain, "3000");
            Actiontype.clickAndWait(Mapper.SelectJoin, "3000");
            Actiontype.DragAndDrop(Mapper.DomainB, Mapper.Dashboard2);
            Thread.Sleep(5000);
            Actiontype.clickAndWait(Mapper.Join, "3000");
            Actiontype.clickAndWait(Mapper.JoinCondition2, "3000");
            Actiontype.clickAndWait(Mapper.PrimaryKey1, "3000");
            Actiontype.clickAndWait(Mapper.PrimaryKey2, "3000");
            Actiontype.clickAndWait(Mapper.AddKey, "3000");
            Actiontype.clickAndWait(Mapper.SaveCondition, "3000");

            Actiontype.SelectDropdownValue(Mapper.JoinType, "TEXT", "Left");
            Thread.Sleep(5000);
            Actiontype.clickAndWait(Mapper.Savebutton, "5000");
        }


        [When(@"I execute mapping")]
        public void WhenIExecuteMapping()
        {
            Actiontype.clickAndWait(Mapper.Execute_Defination, "4000");
            Actiontype.clickAndWait(Mapper.Executeconfirm_Defination, "3000");
        }


        [Then(@"verify mapping status")]
        public void ThenVerifyMappingStatus()
        {
            Actiontype.WaitUntilElementPresent(Mapper.Verify_MappingStatus, 40000);
            Console.WriteLine("Mapper executed successfully");
        }


       
        [Then(@"I should see mapped domain ""(.*)"" in DataStore ""(.*)""")]
        public void ThenIShouldSeeMappedDomainInDataStore(string domain, string DataStore)
        {
            Actiontype.clickAndWait(ExportFile.ExportHeader, "4000");

            String DataStoreType = ExportFile.DataStore.Replace("DataStore", DataStore);
            Actiontype.click(DataStoreType);

            String DomainType = ExportFile.Domain.Replace("Domain", domain);
            Actiontype.VerifyElementPresent(DomainType);
        }


    }
}
