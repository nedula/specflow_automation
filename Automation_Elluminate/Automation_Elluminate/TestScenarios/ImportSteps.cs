﻿using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class Elluminate_ImportSteps
    {
        [Given(@"Go to Data Sources tab")]
        public void GivenGoToDataSourcesTab()
        {
            Actiontype.clickAndWait(DataSources.Datasource, "3000");
            Actiontype.WaitUntilElementPresent(DataSources.New, 120);
            Console.WriteLine("Navigation to Data-Source successful");
        }
        
        [Given(@"Clear the data within Data Sources tab")]
        public void GivenClearTheDataWithinDataSourcesTab()
        {
            Functions.DeleteSource();
        }
        
        [When(@"I go to Importer tab")]
        public void WhenIGoToImporterTab()
        {
            Actiontype.clickAndWait(ImportFile.ImportHeader, "2000");
            //Actiontype.VerifyElementPresent(ImportFile.Verify_ImportPage);
            Console.WriteLine("Navigation to Importer home page was successful");
        }
        
        [Then(@"I Clear the data within Importer tab")]
        public void ThenIClearTheDataWithinImporterTab()
        {
            Functions.DeleteSource();
            Actiontype.clickAndWait(ImportFile.HistoryTab, "2000");
            Functions.DeleteImportHistory();
            Actiontype.clickAndWait(ImportFile.ImportHeader, "2000");
            Actiontype.WaitUntilElementPresent(ImportFile.New, 120);
            Functions.DeleteSource();
        }

        [When(@"I create a new Data Source with below details")]
        public void WhenICreateANewDataSourceWithBelowDetails(Table table)
        {
            Actiontype.clickAndWait(DataSources.New,"2000");
            Actiontype.VerifyElementPresent(DataSources.Header);
            Actiontype.clickAndWait(DataSources.Typedrpdwn, "2000");
            Actiontype.click(DataSources.clkFTP);
            Actiontype.EnterValue(DataSources.Name, table.Rows[0][0].ToString());
            Actiontype.EnterValue(DataSources.AddressUrl, table.Rows[0][1].ToString());
            Actiontype.EnterValue(DataSources.UserName, table.Rows[0][2].ToString());
            Actiontype.EnterValue(DataSources.Password, table.Rows[0][3].ToString());
            Actiontype.EnterValue(DataSources.Remotefoldername, table.Rows[0][4].ToString());
            Actiontype.EnterValue(DataSources.Remotefiles, table.Rows[0][5].ToString());
            Actiontype.EnterValue(DataSources.Description, table.Rows[0][6].ToString());
            Actiontype.clickAndWait(DataSources.Save, "3000");
            Console.WriteLine("Data-Source added successfully");

        }
        [When(@"I create a new ""(.*)"" Data Source with below details")]
        public void WhenICreateANewDataSourceWithBelowDetails(string datasource, Table table)
        {
            Actiontype.clickAndWait(DataSources.New, "2000");
            Actiontype.VerifyElementPresent(DataSources.Header);
            Actiontype.clickAndWait(DataSources.Typedrpdwn, "2000");

            if (datasource.Equals("FTP") || datasource.Equals("FTP Outbound"))
            {
                if (datasource.Equals("FTP"))
                {
                    Actiontype.click(DataSources.clkFTP); 
                }
                else if (datasource.Equals("FTP Outbound"))
                {
                    Actiontype.click(DataSources.clkFTPOutBound);
                }
                
                Actiontype.EnterValue(DataSources.Name, table.Rows[0][0].ToString());
                Actiontype.EnterValue(DataSources.AddressUrl, table.Rows[0][1].ToString());
                Actiontype.EnterValue(DataSources.UserName, table.Rows[0][2].ToString());
                Actiontype.EnterValue(DataSources.Password, table.Rows[0][3].ToString());
                Actiontype.EnterValue(DataSources.Remotefoldername, table.Rows[0][4].ToString());

                if (datasource.Equals("FTP"))
                {
                    Actiontype.EnterValue(DataSources.Remotefiles, table.Rows[0][5].ToString());
                }
                              
                Actiontype.EnterValue(DataSources.Description, table.Rows[0][6].ToString());
               
            }

            if (datasource.Equals("MediroWebService"))
            {
                Actiontype.click(DataSources.clkMedrio);
                Actiontype.EnterValue(DataSources.Name, table.Rows[0][0].ToString());
                Actiontype.EnterValue(DataSources.AddressUrl, table.Rows[0][1].ToString());
                Actiontype.EnterValue(DataSources.apikey, table.Rows[0][2].ToString()); //APIKEY
                Actiontype.EnterValue(DataSources.Description, table.Rows[0][3].ToString());
            }
            if (datasource.Equals("MergeeCLinicalOSWebServices"))
            {
                Actiontype.click(DataSources.clkMergeeclinical);
                Actiontype.EnterValue(DataSources.Name, table.Rows[0][0].ToString());
                Actiontype.EnterValue(DataSources.AddressUrl, table.Rows[0][1].ToString());

                Actiontype.EnterValue(DataSources.UserName, table.Rows[0][2].ToString());
                Actiontype.EnterValue(DataSources.Password, table.Rows[0][3].ToString());
                Actiontype.EnterValue(DataSources.RemoteStudy, table.Rows[0][4].ToString());
                Actiontype.EnterValue(DataSources.Description, table.Rows[0][5].ToString());
            }
            if (datasource.Equals("RaveBioStatsGateway") || datasource.Equals("RaveODMAdaptor") || datasource.Equals("RaveWebServices"))
            {
                if(datasource.Equals("RaveBioStatsGateway"))
                {
                    Actiontype.click(DataSources.clkRaveBiostat);
                }
                if(datasource.Equals("RaveODMAdaptor"))
                {
                    Actiontype.click(DataSources.clkRaveODM);
                }
                if(datasource.Equals("RaveWebServices"))
                {
                    Actiontype.click(DataSources.clkRaveWebService);
                }
                
                Actiontype.EnterValue(DataSources.Name, table.Rows[0][0].ToString());
                Actiontype.EnterValue(DataSources.AddressUrl, table.Rows[0][1].ToString());

                Actiontype.EnterValue(DataSources.UserName, table.Rows[0][2].ToString());
                Actiontype.EnterValue(DataSources.Password, table.Rows[0][3].ToString());
                Actiontype.EnterValue(DataSources.RemoteStudy, table.Rows[0][4].ToString());
                Actiontype.EnterValue(DataSources.Environment, table.Rows[0][5].ToString());
                Actiontype.EnterValue(DataSources.Description, table.Rows[0][6].ToString());
            }
            
            Actiontype.clickAndWait(DataSources.Save, "3000");
            Console.WriteLine("Data-Source added successfully");
        }

        [Then(@"I create import definition for FTP")]
        public void ThenICreateImportDefinitionForFTP()
        {
            Actiontype.clickAndWait(ImportFile.New, "2000");
            Variables.RandomString = "ImportAutomationFTP" + Actiontype.RandomString(5);
            Actiontype.EnterValue(ImportFile.Name, Variables.RandomString);
            Actiontype.click(ImportFile.DataSource);
            Actiontype.EnterValue(ImportFile.DataSourceType, "Auto_DS_FTP");
            Actiontype.click(ImportFile.DSChoice);
            Actiontype.clickAndWait(ImportFile.Save, "10000");
            Console.WriteLine("Import definition added successfully");
        }

        [Then(@"I create import definition for ""(.*)"" with Data Source is ""(.*)"" and DataStore is ""(.*)""")]
        public void ThenICreateImportDefinitionForWithDataSourceIsAndDataStoreIs(string ImportType, string DataSource, string DataStore)
        {
            Actiontype.clickAndWait(ImportFile.New, "2000");
            Variables.RandomString = "ImportAutomation" + ImportType + Actiontype.RandomString(5);
            Actiontype.EnterValue(ImportFile.Name, Variables.RandomString);
            Actiontype.click(ImportFile.DataSource);
            Actiontype.EnterValue(ImportFile.DataSourceType, DataSource);
            ElementInfo.webElement.SendKeys(Keys.Enter);
            Actiontype.click(ImportFile.DataStore);
            Actiontype.EnterValue(ImportFile.DataStoreType, DataStore);
            ElementInfo.webElement.SendKeys(Keys.Enter);           
            Actiontype.clickAndWait(ImportFile.Save, "10000");
            Console.WriteLine("Import definition added successfully");
        }


        [Then(@"I create import definition for MediroWebService")]
        public void ThenICreateImportDefinitionForMediroWebService()
        {
            Actiontype.clickAndWait(ImportFile.New, "2000");
            Variables.RandomString = "ImportAutomationMediroWs" + Actiontype.RandomString(5);
            Actiontype.EnterValue(ImportFile.Name, Variables.RandomString);
            Actiontype.click(ImportFile.DataSource);
            Actiontype.EnterValue(ImportFile.DataSourceType, "Auto_MediroWS_DS");
            Actiontype.click(ImportFile.DSChoice);
            Actiontype.clickAndWait(ImportFile.Save, "10000");
            Console.WriteLine("Import added successfully");
        }
       
        [Then(@"I create import definition for MergeeCLinicalOSWebServices")]
        public void ThenICreateImportDefinitionForMergeeCLinicalOSWebServices()
        {
            Actiontype.clickAndWait(ImportFile.New, "2000");
            Variables.RandomString = "ImportAutomationMergeEclinicalOSWS" + Actiontype.RandomString(5);
            Actiontype.EnterValue(ImportFile.Name, Variables.RandomString);
            Actiontype.click(ImportFile.DataSource);
            Actiontype.EnterValue(ImportFile.DataSourceType, "Auto_MergeEWS_DS");
            Actiontype.click(ImportFile.DSChoice);
            Actiontype.clickAndWait(ImportFile.Save, "10000");
            Console.WriteLine("Import added successfully");
        }

        [Then(@"I create import definition for RaveBioStatsGateway")]
        public void ThenICreateImportDefinitionForRaveBioStatsGateway()
        {
            Actiontype.clickAndWait(ImportFile.New, "2000");
            Variables.RandomString = "ImportAutomationRaveBiostats" + Actiontype.RandomString(5);
            Actiontype.EnterValue(ImportFile.Name, Variables.RandomString);
            Actiontype.click(ImportFile.DataSource);
            Actiontype.EnterValue(ImportFile.DataSourceType, "Auto_RaveBSG_DS");
            Actiontype.click(ImportFile.DSChoice);
            Actiontype.click(ImportFile.DataSourceDeatils);
           // Actiontype.click(ImportFile.Incremental);
            Actiontype.click(ImportFile.IncludeLabView);
            Actiontype.click(ImportFile.metadataversiondropdown);
            Actiontype.click(ImportFile.metadataversion);
            Actiontype.clickAndWait(ImportFile.Save, "10000");
            Console.WriteLine("Import added successfully");
        }

        [Then(@"I create import definition for RaveODMAdaptor")]
        public void ThenICreateImportDefinitionForRaveODMAdaptor()
        {
            Actiontype.clickAndWait(ImportFile.New, "2000");
            Variables.RandomString = "ImportAutomationRaveODMAdaptor" + Actiontype.RandomString(5);
            Actiontype.EnterValue(ImportFile.Name, Variables.RandomString);
            Actiontype.click(ImportFile.DataSource);
            Actiontype.EnterValue(ImportFile.DataSourceType, "Auto_RaveODM_DS");
            Actiontype.click(ImportFile.DSChoice);
            Actiontype.click(ImportFile.DataSourceDeatils);
            Actiontype.click(ImportFile.Audits);
            Actiontype.click(ImportFile.metadataversiondropdown);
            Actiontype.click(ImportFile.metadataversion);
            Actiontype.clickAndWait(ImportFile.Save, "10000");
            Console.WriteLine("Import added successfully");
        }

        [Then(@"I create import definition for RaveWebServices")]
        public void ThenICreateImportDefinitionForRaveWebServices()
        {
            Actiontype.clickAndWait(ImportFile.New, "2000");
            Variables.RandomString = "ImportAutomationRavewedservices" + Actiontype.RandomString(5);
            Actiontype.EnterValue(ImportFile.Name, Variables.RandomString);
            Actiontype.click(ImportFile.DataSource);
            Actiontype.EnterValue(ImportFile.DataSourceType, "Auto_RaveWS_DS");
            Actiontype.click(ImportFile.DSChoice);

            Actiontype.click(ImportFile.DataSourceDeatils);
            // Actiontype.click(ImportFile.Incremental);
            Actiontype.click(ImportFile.IncludeLabView);
            Actiontype.click(ImportFile.metadataversiondropdown);
            Actiontype.click(ImportFile.metadataversion);
            Actiontype.clickAndWait(ImportFile.Save, "10000");
            Console.WriteLine("Import added successfully");
        }

        [Then(@"I create import definition for File")]
        public void ThenICreateImportDefinitionForFile()
        {
            Actiontype.clickAndWait(ImportFile.New, "2000");
            Variables.RandomString = "ImportfileAutomation" + Actiontype.RandomString(5);
            Actiontype.EnterValue(ImportFile.Name, Variables.RandomString);
            Actiontype.click(ImportFile.DataSource);
            Actiontype.EnterValue(ImportFile.DataSourceType, "File");
            Actiontype.click(ImportFile.DSChoice);
            Actiontype.EnterValue(ImportFile.Password, "12345");
            Actiontype.clickAndWait(ImportFile.Save, "10000");
            Console.WriteLine("Import added successfully");
        }

        [Then(@"I Run by uploading file")]
        public void ThenIRunByUploadingFile()
        {
            Actiontype.click(DataSources.DSCheckbox);
            Actiontype.clickAndWait(ImportFile.Run, "5000");
           // Actiontype.clickAndWait(DataSources.DSYesbutton, "10000");
            Actiontype.clickAndWait(ImportFile.Fileupload, "1000");
            Actiontype.Uploadfile(Startupfile.ManualFileImportExcel);
            Actiontype.WaitUntilElementNotPresent(ImportFile.Downloading, 100);
            Actiontype.VerifyElementPresent(ImportFile.ReadyToIntegrate);
            Actiontype.clickAndWait(ImportFile.Integrate, "5000");
            Actiontype.clickAndWait(DataSources.DSYesbutton, "10000");
            Actiontype.WaitUntilElementNotPresent(ImportFile.ArchiveFile, 800);
            Actiontype.VerifyElementPresent(ImportFile.IntegrateComplete);
            Functions.DeleteSource();
        }

        [Then(@"I perform Manual File Import")]
        public void ThenIPerformManualFileImport()
        {
            Actiontype.clickAndWait(ImportFile.manualFileImport, "3000");
            Actiontype.clickAndWait(ImportFile.Fileupload, "1000");
            Actiontype.Uploadfile(Startupfile.ManualFileImportExcel);
            Actiontype.WaitUntilElementNotPresent(ImportFile.Downloading, 100);
            Actiontype.VerifyElementPresent(ImportFile.ReadyToIntegrate);
            Actiontype.clickAndWait(ImportFile.Integrate, "5000");
            Actiontype.clickAndWait(DataSources.DSYesbutton, "10000");
            Actiontype.WaitUntilElementNotPresent(ImportFile.ArchiveFile, 800);
            Actiontype.VerifyElementPresent(ImportFile.IntegrateComplete);
        }
        [Then(@"I Schedule Import for FTP")]
        public void ThenIScheduleImportForFTP()
        {
            Actiontype.click(DataSources.DSCheckbox);
            Actiontype.clickAndWait(ImportFile.Schedule, "5000");
            Actiontype.clickAndWait(ImportFile.Schedulebutton, "5000");
            Actiontype.clickAndWait(ImportFile.ScheduleNext, "5000");
            String SecheduleTime = Actiontype.GetAttributeValue(ImportFile.ScheduleTime, "value");
            DateTime NewSecheduleTime = DateTime.ParseExact(SecheduleTime, "yyyy-MMM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            NewSecheduleTime = NewSecheduleTime.AddHours(-1);
            NewSecheduleTime = NewSecheduleTime.AddMinutes(2);
            String newdate = NewSecheduleTime.ToString("yyyy-MMM-dd HH:mm:ss");
            Actiontype.ClearText(ImportFile.ScheduleTime);
            Actiontype.EnterValue(ImportFile.ScheduleTime, newdate);
            Actiontype.clickAndWait(ImportFile.ScheduleNext, "1000");
            Actiontype.clickAndWait(ImportFile.ScheduleNext, "1000");
            Actiontype.clickAndWait(ImportFile.ScheduleClose, "2000");
            Functions.VeirfyScheduleTask();
        }

       
        [Then(@"Run and clear import definitions")]
        public void ThenRunAndClearImportDefinitions()
        {
            Actiontype.click(DataSources.DSCheckbox);
            Actiontype.clickAndWait(ImportFile.Run, "5000");
            Actiontype.clickAndWait(DataSources.DSYesbutton, "10000");
            Actiontype.WaitUntilElementNotPresent(ImportFile.Downloading, 100); //waiting until downloading should complete
            Actiontype.VerifyElementNotPresent(ImportFile.IntegrateError); // verify Status is error or not 
            Actiontype.VerifyElementPresent(ImportFile.ReadyToIntegrate); // verify status ReadyToIntegrate
            Actiontype.clickAndWait(ImportFile.Integrate, "5000");
            Actiontype.clickAndWait(DataSources.DSYesbutton, "10000");
            Actiontype.WaitUntilElementNotPresent(ImportFile.Uploadingtostaging, 800);
            Actiontype.VerifyElementNotPresent(ImportFile.IntegrateError); // verify Status is error or not 
            Actiontype.VerifyElementPresent(ImportFile.IntegrateComplete); // verify status should be complete 
            Actiontype.clickAndWait(ImportFile.HistoryTab, "5000"); // click on History Tab
            Functions.VerifyImportHistory(); // verify import history details 

            //Go to import Tab and delete import defination and data source
            Actiontype.clickAndWait(ImportFile.ImportHeader, "2000");
            Actiontype.WaitUntilElementPresent(ImportFile.New, 120);

            // verifying and deleting import data
            Functions.DeleteSource();

            // verifying and deleting Data source
            Actiontype.clickAndWait(DataSources.Datasource, "4000");
            Actiontype.WaitUntilElementPresent(DataSources.New, 120);
            Console.WriteLine("Navigation to Data-Source successful");

            Functions.DeleteSource();
        }


        [When(@"I Select import definition and run")]
        public void WhenISelectImportDefinitionAndRun()
        {
            Actiontype.click(DataSources.DSCheckbox);
            Actiontype.clickAndWait(ImportFile.Run, "5000");
            Actiontype.clickAndWait(DataSources.DSYesbutton, "10000");
            Actiontype.WaitUntilElementNotPresent(ImportFile.Downloading, 100); //waiting until downloading should complete
            Actiontype.VerifyElementNotPresent(ImportFile.IntegrateError); // verify Status is error or not 
        }

        [Then(@"Files started Importing and Wait until status is Read to integrate")]
        public void ThenFilesStartedImportingAndWaitUntilStatusIsReadToIntegrate()
        {
            Actiontype.VerifyElementPresent(ImportFile.ReadyToIntegrate); // verify status ReadyToIntegrate
        }

        [Then(@"Click on Integrate button and Wait until status is Completed")]
        public void ThenClickOnIntegrateButtonAndWaitUntilStatusIsCompleted()
        {
            Actiontype.clickAndWait(ImportFile.Integrate, "5000");
            Actiontype.clickAndWait(DataSources.DSYesbutton, "10000");
            Actiontype.WaitUntilElementNotPresent(ImportFile.Uploadingtostaging, 800);
            Actiontype.VerifyElementNotPresent(ImportFile.IntegrateError); // verify Status is error or not 
            Actiontype.VerifyElementPresent(ImportFile.IntegrateComplete); // verify status should be complete 
        }

        [Then(@"Verify import definitions status in History Tab")]
        public void ThenVerifyImportDefinitionsStatusInHistoryTab()
        {
            Actiontype.clickAndWait(ImportFile.HistoryTab, "5000"); // click on History Tab
            Functions.VerifyImportHistory(); // verify import history details 
        }

        [Then(@"I Deleted the Import definition and related data source")]
        public void ThenIDeletedTheImportDefinitionAndRelatedDataSource()
        {
            //Go to import Tab and delete import defination and data source
            Actiontype.clickAndWait(ImportFile.ImportHeader, "2000");
            Actiontype.WaitUntilElementPresent(ImportFile.New, 120);

            // verifying and deleting import data
            Functions.DeleteSource();

            // verifying and deleting Data source
            Actiontype.clickAndWait(DataSources.Datasource, "4000");
            Actiontype.WaitUntilElementPresent(DataSources.New, 120);
            Console.WriteLine("Navigation to Data-Source successful");

            Functions.DeleteSource();
        }



    }
}
