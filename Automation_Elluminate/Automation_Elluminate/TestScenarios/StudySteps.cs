﻿using System;
using TechTalk.SpecFlow;

namespace Automation_Elluminate
{
    [Binding]
    public class StudySteps
    {
        

        [Given(@"We are at the study dashboard page")]
        public void GivenWeAreAtTheStudyDashboardPage()
        {
            Actiontype.clickAndWait(ElluminateHome.StudyHome, "2000");
            Actiontype.VerifyElementPresent(AddStudy.StudiesPage);
        }
        

        [Given(@"I delete a compound called ""(.*)""")]
        public void GivenIDeleteACompoundCalled(string CompoundName)
        {
            Actiontype.clickAndWait(ElluminateHome.Administration, "2000");
            Actiontype.clickAndWait(ElluminateHome.Administation_Configuration, "2000");
            Actiontype.clickAndWait(Compounds.Compoundlink, "2000");
            Functions.DeleteRow( Compounds.CompoundTable, CompoundName);

        }


        [When(@"I add a new study named ""(.*)""")]
        public void WhenIAddANewStudyNamed(string StudyName)
        {
            //Variables.RandomString = "00Automation" + Actiontype.RandomString(5);
            Actiontype.clickAndWait(AddStudy.AddStudylink, "2000");
            Actiontype.EnterValue(AddStudy.StudyId, StudyName);
       
        }
        
        [When(@"I save the study")]
        public void WhenISaveTheStudy()
        {
            Actiontype.clickAndWait(AddStudy.SaveStudy, "2000");
            
        }
        
        [When(@"I delete ""(.*)""")]
        public void WhenIDelete(string p0)
        {
            
            Actiontype.VerifyElementPresent(AddStudy.DeleteStudy);
            Actiontype.clickAndWait(AddStudy.DeleteStudy, "2000");
            Actiontype.clickAndWait(AddStudy.DelStudyCheckbox, "2000");
            Actiontype.clickAndWait(AddStudy.YesButton, "2000");
            Console.WriteLine("Study deleted successfully");
        }
        
       

        [Then(@"I should not see study ""(.*)""")]
        public void ThenIShouldNotSeeStudy(string StudyName)
        {
            Actiontype.EnterValue(AddStudy.searchfilter, StudyName);
            Actiontype.clickAndWait(AddStudy.Searchstudy, "2000");
            String Study = AddStudy.ClickStudy.Replace("Study", StudyName);
            Actiontype.VerifyElementPresent(Study);
        }


        [Then(@"I should see ""(.*)"" on the dashboard\.")]
        public void ThenIShouldSeeOnTheDashboard_(string StudyName)
        {
            Actiontype.clickAndWait(AddStudy.tileview, "3000");
            Actiontype.EnterValue(AddStudy.searchfilter, StudyName);
            Actiontype.clickAndWait(AddStudy.Searchstudy, "2000");
            String Study = AddStudy.ClickStudy.Replace("Study", StudyName);
            Actiontype.VerifyElementPresent(Study);
        }
    }
}
