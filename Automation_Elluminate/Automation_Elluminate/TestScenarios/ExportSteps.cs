﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Threading;
using TechTalk.SpecFlow;

namespace Automation_Elluminate.TestScenarios
{
    [Binding]
    public class Elluminate_ExportSteps
    {
        [Given(@"I should see Study called ""(.*)""")]
        public void GivenIShouldSeeStudyCalled(string StudyName)
        {
            Actiontype.clickAndWait(AddStudy.tileview, "3000");
            Actiontype.EnterValue(AddStudy.searchfilter, StudyName);
            Actiontype.clickAndWait(AddStudy.Searchstudy, "2000");
            String Study = AddStudy.ClickStudy.Replace("Study", StudyName);
            Actiontype.VerifyElementPresent(Study);
        }
        
        [When(@"I select Export Tab from the menu")]
        public void WhenISelectExportTabFromTheMenu()
        {

            Actiontype.clickAndWait(ExportFile.ExportHeader, "5000");
            Actiontype.VerifyElementPresent(ExportFile.SelectDomain2);

        }
        
        [When(@"I Select DataStore is ""(.*)""")]
        public void WhenISelectDataStoreIs(string DataStore)
        {
            String DataStoreType = ExportFile.DataStore.Replace("DataStore", DataStore);
            Actiontype.click(DataStoreType);
        }
        
        [When(@"I Select Available Domains")]
        public void WhenISelectAvailableDomains()
        {
            Actiontype.clickAndWait(ExportFile.SelectDomain2, "2000");
        }

        [When(@"I Select Export FTP Outbound checkbox")]
        public void WhenISelectExportFTPOutboundCheckbox()
        {
            Actiontype.clickAndWait(ExportFile.ExportFTPcheckbox, "5000");
        }


       
        [When(@"I Schedule Task for Export process")]
        public void WhenIScheduleTaskForExportProcess()
        {
            Actiontype.clickAndWait(ExportFile.Schedule, "2000");
            Actiontype.clickAndWait(ImportFile.ScheduleNext, "5000");
            String SecheduleTime = Actiontype.GetAttributeValue(ImportFile.ScheduleTime, "value");
            DateTime NewSecheduleTime = DateTime.ParseExact(SecheduleTime, "yyyy-MMM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            NewSecheduleTime = NewSecheduleTime.AddHours(-1);
            NewSecheduleTime = NewSecheduleTime.AddMinutes(2);
            String newdate = NewSecheduleTime.ToString("yyyy-MMM-dd HH:mm:ss");
            Actiontype.ClearText(ImportFile.ScheduleTime);
            Actiontype.EnterValue(ImportFile.ScheduleTime, newdate);
            Actiontype.clickAndWait(ImportFile.ScheduleNext, "1000");
            Actiontype.clickAndWait(ImportFile.ScheduleNext, "1000");
            Actiontype.StoreText(TaskSchedule.DefaultTaskName, "ScheduleTaskName");
            Actiontype.clickAndWait(ImportFile.ScheduleClose, "2000");

        }


        [When(@"CLick on Export button")]
        public void WhenCLickOnExportButton()
        {
            Actiontype.clickAndWait(ExportFile.Export, "1000");
        }
        
        [Then(@"Verify Exprot process For DataStore ""(.*)"" , Formate as ""(.*)"" and Status ""(.*)""")]
        public void ThenVerifyExprotProcessForDataStoreFormateAsAndStatus(string DataStore, string Formate, string Status)
        {
            Functions.VerifyExportProcess(DataStore, Formate, Status);
        }
        
        [Then(@"Download the exported file")]
        public void ThenDownloadTheExportedFile()
        {
            Actiontype.clickAndWait(ExportFile.Download, "5000");
        }


        [When(@"I Select Data Formate is Delimited with Tilde Delimiter")]
        public void WhenISelectDataFormateIsDelimitedWithTildeDelimiter()
        {
            Actiontype.clickAndWait(ExportFile.Format, "2000");
            Actiontype.clickAndWait(ExportFile.Delimited, "2000");
            Actiontype.clickAndWait(ExportFile.Delimited_Tilde, "2000");
        }

        [When(@"I Select Data Formate is Delimited with Vertical Bar Delimiter")]
        public void WhenISelectDataFormateIsDelimitedWithVerticalBarDelimiter()
        {
            Actiontype.clickAndWait(ExportFile.Format, "2000");
            Actiontype.clickAndWait(ExportFile.Delimited, "2000");
            Actiontype.clickAndWait(ExportFile.Delimited_VerticalBar, "2000");
        }

        [Then(@"Verify Schedule Export Added to task list")]
        public void ThenVerifyScheduleExportAddedToTaskList()
        {
            String TaskName = Actiontype.GetHashKeyValues("${ScheduleTaskName}");

            try
            {

                IList<IWebElement> displayedOptions = ElementInfo.driver.FindElements(By.XPath(TaskSchedule.TaskscheduleTable));

                int count = displayedOptions.Count;

                for (int i = 1; i < count; i++)
                {
                    String temp = ElementInfo.driver.FindElement(By.XPath(TaskSchedule.TaskscheduleTable + "[" + i + "]/td[2]")).Text;

                    if (TaskName == temp)
                    {
                        i = i + count;

                        Console.WriteLine("Verify Schedule Export Added to task list \r\n" + "Expected Task name is available in task list :" + TaskName);
                    }

                }
            }
            catch (Exception)
            {
                Console.WriteLine("Verify Schedule Export Added to task list \r\n" + "Expected Task name  is not available in task list :" + TaskName);
                Variables.isTestCaseFailed = true;
                Report.ErrorMessage = "Verify Schedule Export Added to task list+ Expected Task name is not available in task list :" + TaskName;
                TestReport.Takescreenshot("Failed", "Verify Schedule Export Added to task list + Expected Task name is not available in task list: " + TaskName);
                throw new mkExceptionHandler("VerifyScheduleExportAddedToTaskList", null, "Verify Schedule Export Added to task list+ Expected Task name is not available in task list :" + TaskName);
            }
        }

        [Then(@"Verify Schedule process is executed successfully")]
        public void ThenVerifyScheduleProcessIsExecutedSuccessfully()
        {

            String TaskName = Actiontype.GetHashKeyValues("${ScheduleTaskName}");
            int wait = 0;
            bool isScheduleTaskExecuted = false;

            try
            {

                IList<IWebElement> displayedOptions = ElementInfo.driver.FindElements(By.XPath(TaskSchedule.TaskscheduleTable));

                int count = displayedOptions.Count;

                for (int i = 1; i < count; i++)
                {
                    String temp = ElementInfo.driver.FindElement(By.XPath(TaskSchedule.TaskscheduleTable + "[" + i + "]/td[2]")).Text;

                    if (TaskName == temp)
                    {
                       
                        while (wait <30)
                        {
                            String Result = ElementInfo.driver.FindElement(By.XPath(TaskSchedule.TaskscheduleTable + "[" + i + "]/td[4]")).Text;

                            if (Result == "Succeeded")
                            {
                                isScheduleTaskExecuted = true;
                                i = i + count;
                                break;
                                
                            }
                            else
                            {
                                Thread.Sleep(5000);
                                wait++;
                            }
                        }

                    }

                }

                if (isScheduleTaskExecuted)
                {
                    Console.WriteLine("Verify Schedule process is executed successfully \r\n Expected Task name  is not available in task list :" + TaskName);
                    Elluminate_TaskSchedule_StudyLevelSteps.DeleteScheduleTask(TaskName);
                }
                else
                {

                    throw new mkExceptionHandler("VerifyScheduleProcessIs not Executed", null, "Verify Schedule process is not Executed \r\n Task name is :" + TaskName);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Verify Schedule process is not Executed \r\n Task name is :" + TaskName);
                Variables.isTestCaseFailed = true;
                Report.ErrorMessage = "Verify Schedule process is not Executed \r\n Task name is :" + TaskName;
                TestReport.Takescreenshot("Failed", "Verify Schedule process is not Executed \r\n Task name is :" + TaskName);
                Elluminate_TaskSchedule_StudyLevelSteps.DeleteScheduleTask(TaskName);
                throw new mkExceptionHandler("VerifyScheduleProcessIsExecutedSuccessfully", null, "Verify Schedule process is not Executed \r\n Task name is :" + TaskName);
            }
        }


    }
}
